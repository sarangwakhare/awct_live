var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Quote = /** @class */ (function (_super) {
    __extends(Quote, _super);
    function Quote() {
        return _super.call(this) || this;
    }
    Quote.prototype.Init = function () {
        Order.prototype.Init();
    };
    Quote.prototype.HideConvertToOrderColumn = function () {
        $('#grid tbody tr').each(function () {
            $(this).find("td").each(function () {
                if ($(this).hasClass('grid-action')) {
                    if ($(this).next().children().hasClass("z-active")) {
                        $(this).children().children("ul").children().find(".z-orders").parent().hide();
                    }
                }
            });
            $(this).find("td.IsConvertedToOrder").each(function () {
                if ($(this).children("i").hasClass("z-active")) {
                    $(this).next().children().children("ul").children().find(".z-orders").parent().hide();
                }
            });
        });
        $('#grid th').each(function () {
            if ($.trim($(this).text()) == "Is Converted To Order")
                $(this).hide();
        });
        $('#grid').find(".IsConvertedToOrder").hide();
    };
    return Quote;
}(ZnodeBase));
//# sourceMappingURL=Quote.js.map