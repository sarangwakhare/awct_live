﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Znode.Engine.Api
{
    public static class CustomWebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //Custom Portal Detail Routes
            config.Routes.MapHttpRoute("custom-portal-list", "customportal/list", new { controller = "customportal", action = "getportallist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-portal-detail-list", "customportaldetail/list", new { controller = "customportal", action = "getcustomportaldetaillist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("custom-portal-get", "customportal/getcustomportaldetail/{customPortalDetailId}", new { controller = "customportal", action = "getcustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), customPortalDetailId = @"^\d+$" });
            config.Routes.MapHttpRoute("custom-portal-create", "customportal/create", new { controller = "customportal", action = "insertcustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("custom-portal-update", "customportal/update", new { controller = "customportal", action = "updatecustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });
            config.Routes.MapHttpRoute("custom-portal-delete", "customportal/delete", new { controller = "customportal", action = "deletecustomportaldetail" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("awctpublishproduct-getconfigurableproductviewmodel", "awctpublishproduct/getconfigurableproductviewmodel/{publishProductId}", new { controller = "awctpublishproduct", action = "getconfigurableproductviewmodel" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), publishProductId = @"^\d+$" });
            config.Routes.MapHttpRoute("awctpublishproduct-getpricesizelist", "awctpublishproduct/getpricesizelist/{configurableProductId}", new { controller = "awctpublishproduct", action = "getpricesizelist" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), configurableProductId = @"^\d+$" });

            config.Routes.MapHttpRoute("awctpublishproduct-get", "awctpublishproduct/get/{publishProductId}", new { controller = "awctpublishproduct", action = "getnewpublishproduct" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), publishProductId = @"^\d+$" });
            config.Routes.MapHttpRoute("awctpublishproduct-getextendedproductdetails", "awctpublishproduct/getextendedproductdetails/{publishProductId}", new { controller = "awctpublishproduct", action = "getextendedproductdetails" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), publishProductId = @"^\d+$" });
            config.Routes.MapHttpRoute("awctpublishproduct-getglobalattributedata", "awctpublishproduct/getglobalattributedata/{globalAttributeCodes}", new { controller = "awctpublishproduct", action = "getglobalattributedata" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });

            config.Routes.MapHttpRoute("awctcustomizedform-modelsearch", "awctcustomizedform/modelsearch", new { controller = "awctcustomizedform", action = "modelsearch" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("awctcustomizedform-newcustomerapplication", "awctcustomizedform/newcustomerapplication", new { controller = "awctcustomizedform", action = "newcustomerapplication" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("awctcustomizedform-becomeacontributer", "awctcustomizedform/becomeacontributer", new { controller = "awctcustomizedform", action = "becomeacontributer" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("awctcustomizedform-previewshow", "awctcustomizedform/previewshow", new { controller = "awctcustomizedform", action = "previewshow" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

            config.Routes.MapHttpRoute("awctorder-generateordernumber", "awctorder/generateordernumber", new { controller = "awctorder", action = "generateordernumber" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });


            //Quote Request Routing
            config.Routes.MapHttpRoute("quote-list", "quote/list", new { controller = "quote", action = "list" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute("quote-createquote", "quote/create", new { controller = "quote", action = "create" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("quote-getquotereceipt", "quote/getquotereceipt/{quoteId}/", new { controller = "quote", action = "getquotereceipt" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), quoteId = @"^\d+$" });

            config.Routes.MapHttpRoute("quote-convertquotetoorder", "quote/convertquotetoorder", new { controller = "quote", action = "convertquotetoorder" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });
            config.Routes.MapHttpRoute("quote-getquotedetail", "quote/getquotebyid/{omsQuoteId}/", new { controller = "quote", action = "getquotebyid" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), omsQuoteId = @"^\d+$" });
            config.Routes.MapHttpRoute("quote-lineitems", "quote/getquotelineitembyquoteid/{omsQuoteId}/", new { controller = "quote", action = "GetQuoteLineItemByQuoteId" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Get), omsQuoteId = @"^\d+$" });
            config.Routes.MapHttpRoute("quote-updatequote", "quote/updatequote", new { controller = "quote", action = "updatequote" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Put) });

            config.Routes.MapHttpRoute("quote-getguestquotereceipt", "quote/getguestquotereceipt/", new { controller = "quote", action = "getguestquotereceipt" }, new { httpMethod = new HttpMethodConstraint(HttpMethod.Post) });

        }
    }
}