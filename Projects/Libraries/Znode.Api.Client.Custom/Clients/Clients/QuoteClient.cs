﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Client.Custom.Clients.Clients
{
    public class QuoteClient : BaseClient, IQuoteClient
    {
        public virtual QuoteListModel GetQuoteList(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            string endpoint = QuoteEndpoint.List();
            endpoint += this.BuildEndpointQueryString((ExpandCollection)null, filters, sorts, pageIndex, pageSize);

            ApiStatus status = new ApiStatus();
            QuoteListResponse resourceFromEndpoint = this.GetResourceFromEndpoint<QuoteListResponse>(endpoint, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>() { HttpStatusCode.OK, HttpStatusCode.NoContent };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            QuoteListModel listModel = new QuoteListModel()
            {
                Quotes = resourceFromEndpoint?.QuoteList?.Quotes,
                PortalName = resourceFromEndpoint?.QuoteList?.PortalName
            };
            if (listModel != null)
            {
                int? count = listModel.Quotes?.Count;
                int num = 0;
                if (count.GetValueOrDefault() > num & count.HasValue)
                {
                    listModel.MapPagingDataFromResponse(resourceFromEndpoint);
                }
            }
            return listModel;
        }

        public virtual QuoteCreateModel Create(QuoteCreateModel quoteCreateModel)
        {
            string endpoint1 = QuoteEndpoint.Create();
            ApiStatus status = new ApiStatus();
            CreateQuoteResponse endpoint2 = this.PostResourceToEndpoint<CreateQuoteResponse>(endpoint1, JsonConvert.SerializeObject((object)quoteCreateModel), status);
            //CreateQuoteResponse endpoint2 = PostResourceToEndpoint<CreateQuoteResponse>(endpoint1, JsonConvert.SerializeObject(quoteCreateModel), status);
            Collection <HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>() { HttpStatusCode.OK, HttpStatusCode.Created };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return endpoint2?.Quote;
        }

        public virtual QuoteResponseModel GetQuoteReceipt(int quoteId)
        {
            string quoteReceipt = QuoteEndpoint.GetQuoteReceipt(quoteId);
            ApiStatus status = new ApiStatus();
            QuoteResponse resourceFromEndpoint = this.GetResourceFromEndpoint<QuoteResponse>(quoteReceipt, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>()
      {
        HttpStatusCode.OK,
        HttpStatusCode.NoContent
      };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return resourceFromEndpoint?.QuoteDetails;
        }


        public virtual QuoteResponseModel GetGuestQuoteReceipt(QuoteModel model)
        {
            ZnodeLogging.LogMessage("start GetGuestQuoteReceipt", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

            string endpoint = QuoteEndpoint.GetGuestQuoteReceipt();
            ApiStatus status = new ApiStatus();
            QuoteResponse response = PostResourceToEndpoint<QuoteResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>() { HttpStatusCode.OK, HttpStatusCode.Created };

            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            ZnodeLogging.LogMessage("end GetGuestQuoteReceipt", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            return response?.QuoteDetails;

        }

        public virtual QuoteResponseModel GetQuoteById(int omsQuoteId)
        {
            string quoteById = QuoteEndpoint.GetQuoteById(omsQuoteId);
            ApiStatus status = new ApiStatus();
            QuoteDetailResponse resourceFromEndpoint = this.GetResourceFromEndpoint<QuoteDetailResponse>(quoteById, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>()
      {
        HttpStatusCode.OK,
        HttpStatusCode.NoContent
      };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return resourceFromEndpoint?.QuoteDetail;
        }

        public virtual OrderModel ConvertQuoteToOrder(
          ConvertQuoteToOrderModel convertToOrderModel)
        {
            string order = QuoteEndpoint.ConvertQuoteToOrder();
            ApiStatus status = new ApiStatus();
            OrderResponse endpoint = this.PostResourceToEndpoint<OrderResponse>(order, JsonConvert.SerializeObject((object)convertToOrderModel), status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>()
      {
        HttpStatusCode.OK,
        HttpStatusCode.Created
      };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return endpoint?.Order;
        }

        public virtual List<QuoteLineItemModel> GetQuoteLineItemByQuoteId(
          int omsQuoteId)
        {
            string lineItemByQuoteId = QuoteEndpoint.GetQuoteLineItemByQuoteId(omsQuoteId);
            ApiStatus status = new ApiStatus();
            QuoteLineItemResponse resourceFromEndpoint = this.GetResourceFromEndpoint<QuoteLineItemResponse>(lineItemByQuoteId, status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>()
      {
        HttpStatusCode.OK,
        HttpStatusCode.NoContent
      };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return resourceFromEndpoint?.QuoteLineItems;
        }

        public virtual BooleanModel UpdateQuote(UpdateQuoteModel model)
        {
            string endpoint1 = QuoteEndpoint.UpdateQuote();
            ApiStatus status = new ApiStatus();
            TrueFalseResponse endpoint2 = this.PutResourceToEndpoint<TrueFalseResponse>(endpoint1, JsonConvert.SerializeObject((object)model), status);
            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode>()
      {
        HttpStatusCode.OK
      };
            this.CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return endpoint2.booleanModel;
        }
        
    }
}
