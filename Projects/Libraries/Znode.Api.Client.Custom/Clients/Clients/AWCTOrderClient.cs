﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Engine.Api.Client;
using Znode.Engine.Exceptions;
using Znode.Sample.Api.Model.Responses;

namespace Znode.Api.Client.Custom.Clients.Clients
{
    public class AWCTOrderClient : BaseClient,IAWCTOrderClient
    {
        
        //Get order details by order id.
        public virtual string GenerateOrderNumber()
        {
            //Get Endpoint.
            string endpoint = AWCTOrderEndpoint.GenerateOrderNumber();          

            //Get response
            ApiStatus status = new ApiStatus();
            AWCTOrderNumberResponse response = GetResourceFromEndpoint<AWCTOrderNumberResponse>(endpoint, status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response?.OrderNumber;
        }

    }
}
