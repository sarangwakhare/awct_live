﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;

namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IAWCTOrderClient : IBaseClient
    {
         string GenerateOrderNumber();
    }
}
