﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTWidgetDataAgent : WidgetDataAgent
    {
        private IWebStoreWidgetClient _widgetClient;

        #region Constructor
        public AWCTWidgetDataAgent(IWebStoreWidgetClient widgetClient, IPublishProductClient productClient, IPublishCategoryClient publishCategoryClient, IBlogNewsClient blogNewsClient, IContentPageClient contentPageClient, ISearchClient searchClient) :
            base(widgetClient, productClient, publishCategoryClient, blogNewsClient, contentPageClient, searchClient)
        {
            _widgetClient = GetClient<IWebStoreWidgetClient>(widgetClient);
        }
        #endregion
        
        //Get category products of selected category.
        public override ProductListViewModel GetCategoryProducts(WidgetParameter widgetparameter, int pageNum = 1, int pageSize = 16, int sortValue = 0)
        {
            pageSize = 40;
            string PropertyValue = Convert.ToString(widgetparameter.properties?.Where(x => x.Key.Contains("pageSize")).FirstOrDefault().Value);
            string SearchTermValue = Convert.ToString(widgetparameter.properties?.Where(x => x.Key.Contains("SearchTerm")).FirstOrDefault().Value);

            if (PropertyValue == "0")
            {
                SaveInSession(WebStoreConstants.ViewAllMode, true);
            }
            else
            {
                SaveInSession(WebStoreConstants.ViewAllMode, false);
            }
           
           SaveInSession("SearchTermValue", SearchTermValue);
           
            
            return base.GetCategoryProducts(widgetparameter, pageNum, pageSize, sortValue);
        }

        public override List<Tuple<string, List<KeyValuePair<string, string>>>> SetFilterParameter(int cmsMappingId, string typeOfMapping, Dictionary<string, object> properties, List<FacetViewModel> Facets)
        {
            ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            //Get facet values from querystring.
            Dictionary<string, List<string>> searchValueList = GetFilterData();

            object searchTerm = string.Empty;
            properties?.TryGetValue("SearchTerm", out searchTerm);

            if (Equals(typeOfMapping, ZnodeConstant.Brand))
                searchValueList?.Remove(ZnodeConstant.Brand);
            List<Tuple<string, List<KeyValuePair<string, string>>>> facetFilters = new List<Tuple<string, List<KeyValuePair<string, string>>>>();
            if (HelperUtility.IsNotNull(searchValueList))
            {
                foreach (var item in searchValueList)
                {
                    //nivi code 
                    // to avoid duplicates in filter
                    List<string> data = item.Value.Distinct().ToList();

                    foreach (string value in data)
                    {
                        string facetValue = Facets.FirstOrDefault(x => x.AttributeName == item.Key)?.AttributeValues?.FirstOrDefault(y => y.AttributeValue == value)?.Label;

                        if (string.IsNullOrEmpty(facetValue))
                            facetValue = value;
                        string url = string.Empty;
                        switch (typeOfMapping)
                        {
                            case ZnodeConstant.Brand:
                                url = $"Search?FacetGroup={item.Key}&FacetValue={HttpUtility.UrlEncode(value)}&brandId={cmsMappingId}&IsRemove=true";
                                break;
                            case ZnodeConstant.SearchWidget:
                                url = $"Search?FacetGroup={item.Key}&FacetValue={HttpUtility.UrlEncode(value)}&pageId={cmsMappingId}&IsRemove=true";
                                break;
                            default:
                                url = $"Search?FacetGroup={item.Key}&FacetValue={HttpUtility.UrlEncode(value)}&categoryId={cmsMappingId}&IsRemove=true&SearchTerm={searchTerm}";
                                break;
                        }

                        if (facetFilters.Any(x => x.Item1 == item.Key))
                            facetFilters.Find(x => x.Item1 == item.Key).Item2.Add(new KeyValuePair<string, string>(facetValue, url));
                        else
                            facetFilters.Add(new Tuple<string, List<KeyValuePair<string, string>>>(item.Key, new List<KeyValuePair<string, string>>() { new KeyValuePair<string, string>(facetValue, url) }));
                    }
                }

            }

            ZnodeLogging.LogMessage("Agent method execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return facetFilters;
        }
    }
}
