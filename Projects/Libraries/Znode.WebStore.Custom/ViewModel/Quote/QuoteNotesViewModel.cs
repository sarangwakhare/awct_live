﻿
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class QuoteNotesViewModel : BaseViewModel
    {
        public string UpdatedBy { get; set; }
        public string Notes { get; set; }
    }
}