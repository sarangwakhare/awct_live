﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
   public class ConfigurableProductModel: BaseViewModel
    {
        
        public int ConfigurableProductId { get; set; }
        public string ConfigurableProductName { get; set; }
        public string ConfigurableProductDefaultImage { get; set; }

        public string SEOUrl { get; set; }
        public string ParentPage { get; set; }
        public bool EnableProductCompare { get; set; }

        public bool IsNewProduct { get; set; }
        public int CategoryId { get; set; }
        public List<ConfigurableAssociatedProductDetails> AssociatedProducts { get; set; }

        public List<ConfigurablePriceSizeGroup> PriceSizeList { get; set; }
    }

    public class ConfigurableAssociatedProductDetails
    {
        public int AssociatedProductId { get; set; }
        public string SwatchText { get; set; }
        public string SwatchColor { get; set; }
        public string ProductColor { get; set; }
        public string Image { get; set; }
        public string ImagePath { get; set; }
        public string SwatchImagePath { get; set; }
    }

    public class ConfigurablePriceSizeGroup
    {
     
        public string Size { get; set; }

        public decimal Price { get; set; }
       
    }
}

