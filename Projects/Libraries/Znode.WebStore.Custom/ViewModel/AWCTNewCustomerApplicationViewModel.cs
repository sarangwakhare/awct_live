﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AWCTNewCustomerApplicationViewModel :BaseViewModel
    {
      public string     FirstName { get;set;}
      public string LastName { get;set;}
      public string   City                {get;set;}
      public string StudioName { get;set;}
      public string EmailAddress { get;set;}
      public string PhoneNumber { get;set;}
      public string CellPhoneNumber { get; set; }
      public string MailingAddress { get; set; }
      public string DeliveryAddress { get; set; }
      public string WebsiteAddress { get; set; }
      public bool ReadyToPlaceOrder { get; set; }
      public string StudioAffiliationFile { get; set; }
        
        //public string CellPhoneNumber { get; set; }



    }
}
