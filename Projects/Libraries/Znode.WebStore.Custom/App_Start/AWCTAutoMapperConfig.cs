﻿using AutoMapper;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomizedFormModel;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.Sample.Api.Model.Quote;
using Znode.WebStore.Custom.ViewModel;
using Znode.WebStore.Custom.ViewModel.Quote;
using Znode.Sample.Api.Model.Cart;

namespace Znode.WebStore.Custom
{
    public static class AWCTAutoMapperConfig
    {
        
        public static void Execute()
        {
            Mapper.CreateMap<AssociatedProductDetails, ConfigurableAssociatedProductDetails>().ReverseMap();
            Mapper.CreateMap<PriceSizeGroup, ConfigurablePriceSizeGroup>().ReverseMap();
            Mapper.CreateMap<ConfigurableProductModel, ConfigurableProductViewModel>().ReverseMap();
            Mapper.CreateMap<ProductDetailGridViewModel, ProductDetailGridModel>().ReverseMap();
            Mapper.CreateMap<AWCTProductViewModel, AWCTPublishProductModel>().ReverseMap();
            Mapper.CreateMap<AWCTTruColorViewModel, AWCTTruColorModel>().ReverseMap();
            Mapper.CreateMap<AWCTTruColor, AWCTTruColorList>().ReverseMap();
            Mapper.CreateMap<ShoppingCartModel, AddToCartViewModel>();
            Mapper.CreateMap<AWCTNewCustomerApplicationModel, AWCTNewCustomerApplicationViewModel>().ReverseMap();
            Mapper.CreateMap<AWCTBecomeAContributerModel, AWCTBecomeAContributerViewModel>().ReverseMap();
            Mapper.CreateMap<AWCTModelSearchModel, AWCTModelSearchViewModel>().ReverseMap();
            /*Start Quote Lookup*/
            Mapper.CreateMap<AWCTQuoteLookupModel, AWCTQuoteLookupViewModel>().ReverseMap();
            /*End Quote Lookup*/
            /*Nivi Code start for guard quote*/
            Mapper.CreateMap<QuoteCreateModel, QuoteCreateViewModel>().ReverseMap();
            Mapper.CreateMap<QuoteResponseModel, QuoteResponseViewModel>()
                  .ForMember(d => d.QuoteNoteList, opt => opt.MapFrom(src => src.QuoteHistoryList)).ReverseMap();
            Mapper.CreateMap<QuoteLineItemModel, QuoteLineItemViewModel>().ReverseMap();
            Mapper.CreateMap<QuoteModel, QuoteViewModel>()
                  .ForMember(d => d.TotalAmount, opt => opt.MapFrom(src => @HelperMethods.FormatPriceWithCurrency(src.TotalAmount, src.CultureCode)));
            Mapper.CreateMap<ConvertQuoteToOrderModel, ConvertQuoteToOrderViewModel>()
                 .ForMember(d => d.PaymentDetails, opt => opt.MapFrom(src => src.PaymentDetails)).ReverseMap();

            Mapper.CreateMap<ConvertQuoteToOrderModel, OrdersViewModel>();
            Mapper.CreateMap<OrderHistoryModel, QuoteNotesViewModel>();
            Mapper.CreateMap<SubmitPaymentDetailsModel, SubmitPaymentDetailsViewModel>().ReverseMap();

            Mapper.CreateMap<ShoppingCartItemModel, ProductDetailModel>()
              .ForMember(d => d.Price, opt => opt.MapFrom(src => src.UnitPrice));

            Mapper.CreateMap<CheckoutViewModel, AWCTCheckoutViewModel>();

            Mapper.CreateMap<ConvertQuoteToOrderModel, ShoppingCartModel>().ReverseMap();

            Mapper.CreateMap<AWCTCartModel, AWCTOutOfStockOptions>().ReverseMap();

            //Mapper.CreateMap<ShoppingCartModel, CartViewModel>()
            //  .ForMember(d => d.SubTotal, opt => opt.MapFrom(src => src.SubTotal.ToPriceRoundOff()))
            //    .ForMember(d => d.Total, opt => opt.MapFrom(src => src.Total))
            //    .ForMember(d => d.Discount, opt => opt.MapFrom(src => src.Discount))
            //    .ForMember(d => d.ShippingResponseErrorMessage, opt => opt.MapFrom(src => src.Shipping.ResponseMessage))
            //    .ForMember(d => d.IsValidShippingSetting, opt => opt.MapFrom(src => src.Shipping.IsValidShippingSetting));


            Mapper.CreateMap<OrderModel, OrdersViewModel>()
                 .ForMember(d => d.Total, opt => opt.MapFrom(src => src.Total))
                 .ForMember(d => d.EmailAddress, opt => opt.MapFrom(src => src.BillingAddress.EmailAddress))
                 .ForMember(d => d.PODocumentPath, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.PODocumentPath) ? HelperUtility.GetFilePath(src.PODocumentPath.Replace("~", string.Empty)) : null));
            Mapper.CreateMap<OrdersViewModel, OrderModel>().ReverseMap();


            Mapper.CreateMap<ShoppingCartModel, CartViewModel>()
          .ForMember(d => d.SubTotal, opt => opt.MapFrom(src => src.SubTotal))
            .ForMember(d => d.Total, opt => opt.MapFrom(src => src.Total))
            .ForMember(d => d.Discount, opt => opt.MapFrom(src => src.Discount))
            .ForMember(d => d.ShippingResponseErrorMessage, opt => opt.MapFrom(src => src.Shipping.ResponseMessage))
            .ForMember(d => d.IsValidShippingSetting, opt => opt.MapFrom(src => src.Shipping.IsValidShippingSetting)).ReverseMap();



                Mapper.CreateMap<CartViewModel, ShoppingCartModel>()
    .ForMember(d => d.SubTotal, opt => opt.MapFrom(src => src.SubTotal))
     .ForMember(d => d.Total, opt => opt.MapFrom(src => src.Total))
     .ForMember(d => d.Discount, opt => opt.MapFrom(src => src.Discount)).ReverseMap();


        }
    }
}
