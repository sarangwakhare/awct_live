﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Custom.Controllers
{
    public class AWCTCategoryController : CategoryController
    {
        private readonly ICategoryAgent _categoryAgent;
        private readonly IWidgetDataAgent _widgetDataAgent;
        public AWCTCategoryController(ICategoryAgent categoryAgent, IWidgetDataAgent widgetDataAgent) : base(categoryAgent, widgetDataAgent)
        {
            this._categoryAgent = categoryAgent;
            this._widgetDataAgent = widgetDataAgent;
        }

        /*Nivi Code- Added login user status in cache to display PLP from Cache*/
        [ChildActionOnly]
        //[ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "categoryId;publishState;viewAll;facetGroup;pageSize;pageNumber;sort;isloginuser;")]
        //public ActionResult AWCTCategoryContent(string category, int categoryId = 0, bool? viewAll = false, bool fromSearch = false, string publishState = "PRODUCTION", bool bindBreadcrumb = true, string facetGroup = "", string sort = "", bool isloginuser = false)
        [ZnodePageCache(Duration = 3600, Location = OutputCacheLocation.Server, VaryByParam = "categoryId;publishState;viewAll;facetGroup;pageSize;pageNumber;sort;")]
        public ActionResult AWCTCategoryContent(string category, int categoryId = 0, bool? viewAll = false, bool fromSearch = false, string publishState = "PRODUCTION", bool bindBreadcrumb = true, string facetGroup = "", string sort = "")
        {
            ZnodeLogging.LogMessage("AWCTCategoryContent categoryId Cache param Details-" +  Convert.ToString(categoryId) + Convert.ToString(publishState) + Convert.ToString(viewAll)
                             + Convert.ToString(facetGroup) + Convert.ToString(sort), "Custom", System.Diagnostics.TraceLevel.Error);
            ZnodeLogging.LogMessage("AWCTCategoryContent categoryId Parameter Details-" + Convert.ToString(category) + Convert.ToString(fromSearch) + Convert.ToString(bindBreadcrumb)
                           , "Custom", System.Diagnostics.TraceLevel.Error);
            //ZnodeLogging.LogMessage("AWCTCategoryContent category"+Convert.ToString(category), "Custom", System.Diagnostics.TraceLevel.Error);
            //ZnodeLogging.LogMessage("AWCTCategoryContent IsloginUser-"+Convert.ToString(isloginuser), "Custom", System.Diagnostics.TraceLevel.Error);
            if (viewAll.Value)
                SessionHelper.SaveDataInSession<bool>(WebStoreConstants.ViewAllMode, true);
            else
                SessionHelper.RemoveDataFromSession(WebStoreConstants.ViewAllMode);

            CategoryViewModel ViewModel = _categoryAgent.GetCategorySeoDetails(categoryId, bindBreadcrumb);

            if (bindBreadcrumb)
                _categoryAgent.GetBreadCrumb(ViewModel);

            if (!fromSearch)
                //Remove Facet from Session.
                _categoryAgent.RemoveFromSession(ZnodeConstant.FacetsSearchFields);

            if (HelperUtility.IsNotNull(ViewModel.SEODetails))
            {
                ViewBag.Title = ViewModel.SEODetails.SEOTitle;
                TempData["Title"] = ViewModel.SEODetails.SEOTitle;
                TempData["Keywords"] = ViewModel.SEODetails.SEOKeywords;
                TempData["Description"] = ViewModel.SEODetails.SEODescription;
                TempData["CanonicalURL"] = ViewModel.SEODetails.CanonicalURL;
                TempData["RobotTag"] = string.IsNullOrEmpty(ViewModel.SEODetails.RobotTag) || ViewModel.SEODetails.RobotTag.ToLower() == "none" ? string.Empty : ViewModel.SEODetails.RobotTag?.Replace("_", ",");
            }
            Dictionary<string, object> searchProperties = GetSearchProperties();
            ViewModel.ProductListViewModel = _widgetDataAgent.GetCategoryProducts(new WidgetParameter { CMSMappingId = categoryId, TypeOfMapping = ViewModel.CategoryName, LocaleId = PortalAgent.LocaleId, properties = searchProperties }
            );

            return PartialView("_ProductListContent", ViewModel);
        }

    }
}
