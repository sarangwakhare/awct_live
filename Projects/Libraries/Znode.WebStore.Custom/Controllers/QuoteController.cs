﻿using System;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.Helpers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;
using Znode.WebStore.Custom.ViewModel.Quote;

namespace Znode.WebStore.Custom.Controllers
{
    public class QuoteController : BaseController
    {
        #region Private Read-only members
        private readonly IQuoteAgent _quoteAgent;
        private readonly ICartAgent _cartAgent;
        private readonly ICheckoutAgent _checkoutAgent;
        private const string singlePageCheckoutView = "~/Views/Themes/{0}/Views/Checkout/SinglePageQuote.cshtml";
        private readonly string QuoteRequestHistoryView = "QuoteRequestHistory";
        private readonly string CheckoutReceipt = "CheckoutReciept";
        #endregion

        #region Public Constructor
        public QuoteController(IQuoteAgent quoteAgent, ICartAgent cartAgent, ICheckoutAgent checkoutAgent)
        {
            _quoteAgent = quoteAgent;
            _cartAgent = cartAgent;
            _checkoutAgent = checkoutAgent;
        }
        #endregion
        // GET: Checkout
        public virtual ActionResult Index()
        {
            if (!Equals(Request.QueryString["ShippingId"], null))
            {
                _cartAgent.AddEstimatedShippingIdToCartViewModel(int.Parse(Convert.ToString(Request.QueryString["ShippingId"])));
            }

            if (User.Identity.IsAuthenticated || !(User.Identity.IsAuthenticated))
            {
                if (_cartAgent.GetCartCount() < 1)
                { return RedirectToAction<HomeController>(x => x.Index()); }

                AWCTCheckoutViewModel model = _checkoutAgent.GetUserDetails().ToModel<AWCTCheckoutViewModel>();
                model.IsQuoteRequest = true;
                //model.InHandDate = DateTime.Now.AddDays(WebStoreConstants.DefaultInHandDays);

                return View(string.Format(singlePageCheckoutView, PortalAgent.CurrentPortal.Theme), model);
            }


            return RedirectToAction("Login", "User", new { returnUrl = "~/quote" });
        }

        public virtual ActionResult SubmitQuote(QuoteCreateViewModel quoteCreateViewModel)
        {
            QuoteCreateViewModel quote = _quoteAgent.Create(quoteCreateViewModel);

            if (HelperUtility.IsNotNull(quote) && !quote.HasError)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    HttpContext.Response.Cookies[WebStoreConstants.CartCookieKey].Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddDays(-1);
                    HttpContext.Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                }
                SetNotificationMessage(GetSuccessNotificationMessage(WebStore_Resources.QuoteCreateSuccessMessage));
                return RedirectToAction<QuoteController>(x => x.GetQuoteReceipt(quote.OmsQuoteId));
            }
            SetNotificationMessage(GetErrorNotificationMessage(!string.IsNullOrEmpty(quote?.ErrorMessage) ? quote?.ErrorMessage : WebStore_Resources.QuoteCreateErrorMessage));
            return RedirectToAction<QuoteController>(x => x.Index());
        }

        ////Quote receipt after successful quote creation.
        public virtual ActionResult GetQuoteReceipt(int quoteId)
        {
            if (User.Identity.IsAuthenticated || !User.Identity.IsAuthenticated)
            {
                if (quoteId > 0)
                {
                    QuoteResponseViewModel quoteResponseViewModel = _quoteAgent.GetQuoteReceipt(quoteId);
                    return HelperUtility.IsNotNull(quoteResponseViewModel) ? View("_QuoteReceipt", quoteResponseViewModel) : RedirectToAction<HomeController>(x => x.Index());
                }
            }
            return RedirectToAction<QuoteController>(x => x.Index());
        }

        //Quote receipt after successful quote creation.
        public virtual ActionResult GetQuote(int omsQuoteId)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (omsQuoteId > 0)
                {
                    QuoteResponseViewModel quoteResponseViewModel = _quoteAgent.GetQuote(omsQuoteId);
                    return HelperUtility.IsNotNull(quoteResponseViewModel) ? View("QuoteDetails", quoteResponseViewModel) : RedirectToAction<HomeController>(x => x.Index());
                }
            }
            return RedirectToAction<QuoteController>(x => x.Index());
        }

        //Convert quote to an order
        [HttpPost]
        public virtual ActionResult ConvertQuoteToOrder(ConvertQuoteToOrderViewModel convertToOrderViewModel)
        {
            if (HelperUtility.IsNotNull(convertToOrderViewModel))
            {
                OrdersViewModel order = _quoteAgent.ConvertQuoteToOrder(convertToOrderViewModel);
                if (order.OmsOrderId > 0)
                {
                    // Below code is used, for after successfully payment from "Credit Card" return receipt.
                    if (IsCreditCardPayment(convertToOrderViewModel?.PaymentDetails.PaymentType))
                    {
                        return Json(new { receiptHTML = RenderRazorViewToString(CheckoutReceipt, order), omsOrderId = order.OmsOrderId });
                    }

                    return RedirectToAction<CheckoutController>(x => x.OrderCheckoutReceipt());
                }

                // Return error message, if payment through "Credit Card" raises any error.
                if (IsCreditCardPayment(convertToOrderViewModel?.PaymentDetails.PaymentType))
                {
                    return Json(new { error = order.ErrorMessage });
                }

                SetNotificationMessage(GetErrorNotificationMessage(WebStore_Resources.ConvertQuoteToOrderErrorMessage));
            }
            return RedirectToAction<QuoteController>(x => x.GetQuote(convertToOrderViewModel.OmsQuoteId));
        }

        //Get quote history 
        public virtual ActionResult QuoteHistory([System.Web.Http.ModelBinding.ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model, string QuoteNumber)
        {
            model.Filters = new FilterCollection();

            if (User.Identity.IsAuthenticated)
            {
                model.Filters.RemoveAll(x => x.Item1 == "QuoteNumber");
                if (!string.IsNullOrEmpty(QuoteNumber))
                {
                    model.Filters.Add(new FilterTuple("QuoteNumber", FilterOperators.Equals, "'" + Convert.ToString(QuoteNumber) + "'"));
                }


                //Get Quote list.
                QuoteListViewModel quoteListViewModel = _quoteAgent.GetQuoteList(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
                model.Filters = quoteListViewModel.Filters;
                int RecordPerPage = model.RecordPerPage > 0 ? model.RecordPerPage : 10;
                //Get the grid model.
                quoteListViewModel.GridModel = FilterHelpers.GetDynamicGridModel(model, quoteListViewModel?.Quotes, "ZnodeWebStoreQuoteRequests".ToString(), string.Empty, null, true, true, quoteListViewModel?.GridModel?.FilterColumn?.ToolMenuList);
                quoteListViewModel.GridModel.RowPerPageCount = RecordPerPage;
                //quoteListViewModel.GridModel.RowCount = quoteListViewModel.GridModel.TotalRowCount;
                //FilterHelpers.GetDynamicGridModel(model, quoteListViewModel?.Quotes, "ZnodeWebStoreQuoteRequests", string.Empty, null, true, true, null);
                quoteListViewModel.GridModel.TotalRecordCount = quoteListViewModel.TotalResults;
                //quoteListViewModel.GridModel.RowCount= quoteListViewModel.GridModel
                //Returns the Account Quote list.
                return ActionView(QuoteRequestHistoryView, quoteListViewModel);
            }
            return RedirectToAction("Login", "User", new { returnUrl = "~/quote/quotehistory" });
        }
        [HttpPost]
        public virtual ActionResult GetGuestQuoteReceipt(string QuoteNumber, string EmailId)
        {
            QuoteResponseViewModel quoteResponseViewModel = new QuoteResponseViewModel();
            if (!string.IsNullOrEmpty(QuoteNumber) && !string.IsNullOrEmpty(EmailId))
            {
                quoteResponseViewModel = _quoteAgent.GetGuestQuoteReceipt(QuoteNumber, EmailId);
                //    return HelperUtility.IsNotNull(quoteResponseViewModel.ShoppingCartItems) 
                //    ? View("_QuoteReceipt", quoteResponseViewModel) :RedirectToAction<HomeController>(x => x.Index());
                if (HelperUtility.IsNull(quoteResponseViewModel.ShoppingCartItems))
                {
                    SetNotificationMessage(GetErrorNotificationMessage("Quote Number is not found."));
                    return View("../Quote/_GetQuote");
                }

            }
            return View("_QuoteReceipt", quoteResponseViewModel);

        }
        public virtual ActionResult QuoteLookup()
        {
            return View("../Quote/_GetQuote");

        }
        #region private method
        //check if payment is Credit Card type
        private bool IsCreditCardPayment(string paymentType)
        {
            return !string.IsNullOrEmpty(paymentType) && Equals(paymentType.ToLower(), ZnodeConstant.CreditCard.ToLower());
        }
        #endregion
    }
}
