﻿
using System;
using System.Configuration;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Core.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Microsoft.AspNet.Identity;


namespace Znode.WebStore.Custom.Controllers
{
    public class AWCTCartController : CartController
    {
        #region Private Variables
        private readonly IAWCTCartAgent _cartAgent;
        private readonly IWSPromotionAgent _promotionAgent;
        private readonly string shoppingCartView = "Cart";
        //private readonly string shoppingCart = "_shoppingCart";
        #endregion
        public AWCTCartController(IAWCTCartAgent cartAgent, IPortalAgent portalAgent, IWSPromotionAgent promotionAgent) : base(cartAgent, portalAgent, promotionAgent)
        {
            _cartAgent = cartAgent;
            _promotionAgent = promotionAgent;
        }
        /*Original Code working for dance*/
        //[Authorize]
        //public override ActionResult Index()
        //{          
        //    CartViewModel cartdata = _cartAgent.GetCart(false);          
        //    cartdata.ShippingModel = _promotionAgent.GetPromotionListByPortalId(PortalAgent.CurrentPortal.PortalId);            
        //    return View(shoppingCartView, cartdata);
        //}
        public override ActionResult Index()
        {
            /*Nivi Code start for guard quote*/
            int PortalID = Convert.ToInt32(ConfigurationManager.AppSettings["PortalID"].ToString());
            bool IsLoginUser = Convert.ToBoolean(SessionProxyHelper.IsLoginUser());
            //UserViewModel userModel = SessionHelper.GetDataFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);
            string UserName = User.Identity.GetUserId() == "" ? "" : User.Identity.GetUserName();

            CartViewModel cartdata = _cartAgent.GetCart(false);
            cartdata.ShippingModel = _promotionAgent.GetPromotionListByPortalId(PortalAgent.CurrentPortal.PortalId);
            if (Convert.ToInt32(PortalID) == PortalAgent.CurrentPortal.PortalId && (!IsLoginUser || string.IsNullOrEmpty(UserName)))//guard login and guest
            {/*7=7 and useris not loggedin */
                return RedirectToAction("Login", "User", new { returnUrl = "~/cart" });
            }

            //if (Convert.ToInt32(PortalID) != PortalAgent.CurrentPortal.PortalId && !IsLoginUser)//guard login and guest
            //{
            //    return View(shoppingCartView, cartdata);
            //}
            //else if (Convert.ToInt32(PortalID) != PortalAgent.CurrentPortal.PortalId && !IsLoginUser)//dance login and guest
            //{
            //    return RedirectToAction("Login", "User", new { returnUrl = "~/cart" });
            //}
            ///*Dance Without Login if user direct wants to go the cart*/
            //else if (Convert.ToInt32(PortalID) == PortalAgent.CurrentPortal.PortalId && !IsLoginUser)
            //{
            //    return RedirectToAction("Login", "User", new { returnUrl = "~/cart" });
            //}
            //else
            //{

            return View(shoppingCartView, cartdata);
            //}
            
        }
        public override ActionResult GetShoppingCart()
        {           
            CartViewModel cartdata = _cartAgent.GetCart();
            cartdata.ShippingModel = _promotionAgent.GetPromotionListByPortalId(PortalAgent.CurrentPortal.PortalId);
            return View(shoppingCartView, cartdata);
        }
       
    }
}
