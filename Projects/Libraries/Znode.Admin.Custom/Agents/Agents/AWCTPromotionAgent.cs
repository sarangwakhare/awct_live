﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Extensions;
using Znode.Engine.Admin.Helpers;
using Znode.Engine.Admin.Models;
using Znode.Engine.Admin.ViewModels;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Admin.Custom.Agents.Agents
{
    public class AWCTPromotionAgent : PromotionAgent
    {
        private readonly IPromotionClient _promotionClient;
        public AWCTPromotionAgent(IPromotionClient promotionClient, IPromotionTypeClient promotionTypeClient, IPortalClient portalClient, IProfileClient profileClient, IEcommerceCatalogClient ecommerceCatalogClient,
           IPortalProfileClient portalProfileClient, IPublishProductClient publishProductClient, IPublishCategoryClient categoryClient, IPublishCatalogClient catelogClient)
            : base(promotionClient, promotionTypeClient, portalClient, profileClient, ecommerceCatalogClient,
 portalProfileClient, publishProductClient, categoryClient, catelogClient)
        {
            _promotionClient = GetClient<IPromotionClient>(promotionClient);
        }
        //Create promotion.
        public override PromotionViewModel CreatePromotion(PromotionViewModel viewModel, BindDataModel bindDataModel)
        {
            try
            {
                ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
                viewModel.LocaleId = Convert.ToInt32(DefaultSettingHelper.DefaultLocale);
                ZnodeLogging.LogMessage("LocaleId of PromotionViewModel: ", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new { LocaleId = viewModel?.LocaleId });
                PromotionViewModel promotionViewModel = GetPromotionViewModel(viewModel, bindDataModel);
                if (!string.IsNullOrEmpty(promotionViewModel.CallForPriceMessage))
                    promotionViewModel.PromotionMessage = promotionViewModel.CallForPriceMessage;
                return _promotionClient.CreatePromotion(promotionViewModel.ToModel<PromotionModel>()).ToViewModel<PromotionViewModel>();
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AlreadyExist:
                        return new PromotionViewModel { HasError = true, ErrorMessage = Admin_Resources.AlreadyExistsCouponCode };
                    case ErrorCodes.CreationFailed:
                        return new PromotionViewModel { HasError = true, ErrorMessage = Admin_Resources.GenerateCouponCode };
                    default:
                        return new PromotionViewModel { HasError = true, ErrorMessage = Admin_Resources.ErrorFailedToCreate };
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Error);
                return new PromotionViewModel { HasError = true, ErrorMessage = Admin_Resources.ErrorFailedToCreate };
            }
        }

        //Update promotion
        public override PromotionViewModel UpdatePromotion(PromotionViewModel viewModel, BindDataModel bindDataModel)
        {
            try
            {
                ZnodeLogging.LogMessage("Agent method execution started.", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Info);
                viewModel.LocaleId = Convert.ToInt32(DefaultSettingHelper.DefaultLocale);
                ZnodeLogging.LogMessage("LocaleId of PromotionViewModel: ", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new { LocaleId = viewModel?.LocaleId });
                PromotionViewModel promotionViewModel = GetPromotionViewModel(viewModel, bindDataModel);
                if (!string.IsNullOrEmpty(promotionViewModel.CallForPriceMessage))
                    promotionViewModel.PromotionMessage = promotionViewModel.CallForPriceMessage;
                PromotionModel promotionModel = _promotionClient.UpdatePromotion(promotionViewModel.ToModel<PromotionModel>());
                return IsNotNull(promotionModel) ? promotionModel.ToViewModel<PromotionViewModel>() : (PromotionViewModel)GetViewModelWithErrorMessage(new PromotionViewModel(), Admin_Resources.UpdateErrorMessage);
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.AlreadyExist:
                        return (PromotionViewModel)GetViewModelWithErrorMessage(viewModel, Admin_Resources.AlreadyExistsCouponCode);
                    case ErrorCodes.InternalItemNotUpdated:
                        return (PromotionViewModel)GetViewModelWithErrorMessage(viewModel, Admin_Resources.AlreadyExistsCouponCode);

                    default:
                        return (PromotionViewModel)GetViewModelWithErrorMessage(viewModel, Admin_Resources.UpdateErrorMessage);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Error);
                return (PromotionViewModel)GetViewModelWithErrorMessage(viewModel, Admin_Resources.UpdateErrorMessage);
            }
        }
        //Get attribute Data
        private PromotionViewModel GetPromotionViewModel(PromotionViewModel viewModel, BindDataModel bindDataModel)
        {
            PromotionViewModel promotionViewModel = new PromotionViewModel();
            promotionViewModel = viewModel;
            promotionViewModel.IsUnique = viewModel.PromotionId > 0 ? (Equals(Convert.ToBoolean(bindDataModel.GetValue("hdnIsUnique")), "") ? false : Convert.ToBoolean(bindDataModel.GetValue("hdnIsUnique"))) : viewModel.IsUnique;
            promotionViewModel.CouponList = GetCouponList(bindDataModel, viewModel);
            promotionViewModel.PromotionTypeName = Convert.ToString(bindDataModel.GetValue("PromotionTypeName"));
            promotionViewModel.PortalId = Convert.ToInt32(bindDataModel.GetValue("hdnPortalIds"));
            RemoveNonAttributeKeys(bindDataModel);
            promotionViewModel.QuantityMinimum = Convert.ToInt32(bindDataModel.GetValue("MinimumQuantity_0_0_0_0_attr"));
            promotionViewModel.OrderMinimum = Convert.ToDecimal(bindDataModel.GetValue("MinimumOrderAmount_0_0_0_0_attr"));
            promotionViewModel.Discount = Convert.ToDecimal(bindDataModel.GetValue("DiscountAmount_0_0_0_0_attr"));
            promotionViewModel.AssociatedCatelogIds = Convert.ToString(bindDataModel.GetValue("hdnPromotionCatalogs_attr"));
            promotionViewModel.AssociatedCategoryIds = Convert.ToString(bindDataModel.GetValue("hdnPromotionCategories_attr"));
            promotionViewModel.AssociatedProductIds = Convert.ToString(bindDataModel.GetValue("hdnPromotionProducts_attr"));
            promotionViewModel.AssociatedBrandIds = Convert.ToString(bindDataModel.GetValue("hdnPromotionBrands_attr"));
            promotionViewModel.AssociatedShippingIds = Convert.ToString(bindDataModel.GetValue("hdnPromotionShippings_attr"));
            promotionViewModel.ReferralPublishProductId = Convert.ToInt32(bindDataModel.GetValue("hdnReferalProductId_attr"));
            promotionViewModel.PromotionProductQuantity = Convert.ToDecimal(bindDataModel.GetValue("ProductQuantity_0_0_0_0_attr"));
            promotionViewModel.CallForPriceMessage = Convert.ToString(bindDataModel.GetValue("CallForPriceMessage_0_0_0_0_attr"));
            if (viewModel.PromotionId > 0 && IsNull(promotionViewModel.PromotionTypeId))
                promotionViewModel.PromotionTypeId = Convert.ToInt32(bindDataModel.GetValue("ddlPromotionType_attr").ToString().Split(',').FirstOrDefault());
            return promotionViewModel;
        }
        // Get coupon list from binddata model.
        private CouponListViewModel GetCouponList(BindDataModel bindDataModel, PromotionViewModel viewModel)
        {
            CouponListViewModel couponListViewModel = new CouponListViewModel() { CouponList = new List<CouponViewModel>() };
            string couponCode = viewModel.IsUnique && !viewModel.PromotionTypeName.Contains("AWCT")? Convert.ToString(bindDataModel.GetValue("MultipleCouponCode")) : Convert.ToString(bindDataModel.GetValue("CouponCode"));
            string customCouponCode = Convert.ToString(bindDataModel.GetValue("CustomCouponCode"));
            ZnodeLogging.LogMessage("couponCode and customCouponCode: ", ZnodeLogging.Components.Marketing.ToString(), TraceLevel.Verbose, new { couponCode = couponCode, customCouponCode = customCouponCode });
            if (!string.IsNullOrEmpty(couponCode))
            {
                List<string> couponCodeList = couponCode.Split(',')?.ToList();
                foreach (string item in couponCodeList)
                {
                    if (!string.IsNullOrEmpty(item))
                        couponListViewModel.CouponList.Add(new CouponViewModel
                        {
                            AvailableQuantity = Equals(Convert.ToString(bindDataModel.GetValue("AvailableQuantity")), "") ? 0 : Convert.ToInt32(bindDataModel.GetValue("AvailableQuantity")),
                            InitialQuantity = Equals(Convert.ToString(bindDataModel.GetValue("InitialQuantity")), "") ? 0 : Convert.ToInt32(bindDataModel.GetValue("InitialQuantity")),
                            Code = item,
                            IsActive = (viewModel.IsUnique && !viewModel.PromotionTypeName.Contains("AWCT")) ? (!string.IsNullOrEmpty(Convert.ToString(bindDataModel.GetValue("isActiveCoupon_" + item))) ? true : false) : true,
                            IsCustomCoupon = string.IsNullOrEmpty(customCouponCode) ? false : true,
                            CustomCouponCode = customCouponCode
                        });
                }
            }
            return couponListViewModel;
        }
    }
}
