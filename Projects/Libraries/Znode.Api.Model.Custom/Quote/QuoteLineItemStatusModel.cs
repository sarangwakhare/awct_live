﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteLineItemStatusModel : BaseModel
    {
        public int? ParentQuoteLineItemId { get; set; }

        public string Message { get; set; }

        public bool Status { get; set; }
    }
}
