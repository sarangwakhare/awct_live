﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class QuoteLineItemStatusListModel : BaseListModel
    {
        public List<QuoteLineItemStatusModel> QuoteLineItemStatusList { get; set; }

        public QuoteLineItemStatusListModel()
        {
            this.QuoteLineItemStatusList = new List<QuoteLineItemStatusModel>();
        }
    }
}
