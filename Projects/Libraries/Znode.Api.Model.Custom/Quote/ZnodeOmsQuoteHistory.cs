﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model.Quote
{
    public class ZnodeOmsQuoteHistory:BaseModel
    {
        public int OmsQuoteHistoryId { get; set; }
        public int? OMSQuoteId { get; set; }
        public decimal? OrderAmount { get; set; }
        public string Message { get; set; }     

        public virtual ZnodeOmsQuote ZnodeOmsQuote { get; set; }
    }
}
