﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Resources;

namespace Znode.Sample.Api.Model.Quote
{
    public class ConvertQuoteToOrderModel : BaseModel
    {
        [Required(ErrorMessageResourceName = "ErrorQuoteIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        [RegularExpression("^[1-9]\\d*$", ErrorMessageResourceName = "ErrorQuoteIdRequired", ErrorMessageResourceType = typeof(Api_Resources))]
        public int OmsQuoteId { get; set; }

        public int UserId { get; set; }

        public int PortalId { get; set; }

        public SubmitPaymentDetailsModel PaymentDetails { get; set; }
    }
}
