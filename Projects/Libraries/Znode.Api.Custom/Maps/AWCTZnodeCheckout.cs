﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;


namespace Znode.Api.Custom.Maps
{
    public class AWCTZnodeCheckout : ZnodeCheckout
    {
        private readonly IZnodeOrderHelper orderHelper;
        private readonly int _ShippingID = 0;

        public AWCTZnodeCheckout()
        {
            orderHelper = GetService<IZnodeOrderHelper>();
            //publishProductHelper = GetService<IPublishProductHelper>();
        }

        // Initializes a new instance of the ZNodeCheckout class.
        public AWCTZnodeCheckout(UserAddressModel userAccount, ZnodePortalCart shoppingCart)
        {

            this.UserAccount = userAccount;
            this.ShoppingCart = shoppingCart;
            orderHelper = GetService<IZnodeOrderHelper>();
            //publishProductHelper = GetService<IPublishProductHelper>();
        }

        ///*Nivi Code commented for AWCT- 475 for penny issue.*/
        //public override ZnodeOrderFulfillment SubmitOrder(SubmitOrderModel model, ShoppingCartModel shoppingCartModel)
        //{
        //    int portalID = ShoppingCart.PortalID;
        //    int orderId = model?.OrderId.GetValueOrDefault() ?? 0;
        //    int orderDetailId = 0;
        //    ZnodeOrderFulfillment order = this.GetOrderFullfillment(this.UserAccount, this.ShoppingCart, portalID);
        //    ///*Nivi Code start for Penny issue.*/
        //    order.Total = Convert.ToDecimal(shoppingCartModel.Total);
        //    ///*Nivi Code end for Penny issue.*/
        //    ///
        //    order.Order.OmsOrderDetailsId = model?.OmsOrderDetailsId == null ? 0 : Convert.ToInt32(model?.OmsOrderDetailsId);

        //    if (IsNotNull(shoppingCartModel?.Payment?.PaymentStatusId))
        //    {
        //        order.PaymentStatusID = shoppingCartModel.Payment.PaymentStatusId;
        //    }

        //    if (orderId > 0)
        //    {
        //        SetOrderStateTrackingNumber(order, model);
        //    }

        //    //start transaction
        //    using (SqlConnection connection = new SqlConnection(Libraries.Data.Helpers.HelperMethods.ConnectionString))
        //    {
        //        connection.Open();     // create order object
        //        SqlTransaction transaction = connection.BeginTransaction();// Start a local transaction.

        //        try
        //        {
        //            if (orderId > 0 && !CancelExistingOrder(order, orderId))
        //            {
        //                return order;
        //            }

        //            SetOrderAdditionalDetails(order, model);

        //            bool paymentIsSuccess = SetPaymentDetails(order);
        //            order.Order.OrderNumber = model?.OrderNumber;

        //            // Add the order and line items to database
        //            order.AddOrderToDatabase(order, shoppingCartModel);
        //            if (orderId > 0)
        //            {
        //                //to save return items in data base for selected order
        //                if (IsNotNull(model?.ReturnOrderLineItems))
        //                {
        //                    this.IsSuccess = SaveReturnItems(order.Order.OmsOrderDetailsId, model.ReturnOrderLineItems);
        //                }
        //                ZnodeLogging.LogMessage($"Updated existing order for Order Id:{orderId }", ZnodeLogging.Components.OMS.ToString());
        //            }

        //            //to get current orderdetailid for verifing  order process
        //            orderDetailId = order?.Order?.OmsOrderDetailsId ?? 0;

        //            //Set Order Shipment Details to order Line Item.
        //            SetOrderShipmentDetails(order);

        //            if (paymentIsSuccess)
        //            {
        //                SetOrderDetailsToShoppingCart(order);
        //                //to apply promotion, taxes and shipping calculation 
        //                this.ShoppingCart.PostSubmitOrderProcess(orderId, shoppingCartModel.ShippingAddress.IsGuest);

        //                int? userId = this.ShoppingCart.GetUserId();

        //                //to save referral commission and gift card history
        //                this.SaveReferralCommissionAndGiftCardHistory(order, userId);

        //                //reduce product inventory this code block move out of transaction because all db call of entity type and this is sp call
        //                if (this.ManageOrderInventory(order, this.ShoppingCart))
        //                {
        //                    transaction.Commit();
        //                    this.IsSuccess = true;
        //                }
        //                else
        //                {
        //                    transaction.Rollback();
        //                    this.IsSuccess = false;
        //                }
        //            }
        //            else
        //            {
        //                //// payment submission failed so rollback transaction
        //                transaction.Rollback();
        //                this.IsSuccess = false;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ZnodeLogging.LogMessage(ex.Message, string.Empty, TraceLevel.Error, ex);

        //            if (IsNotNull(ex.InnerException))
        //            {
        //                ZnodeLogging.LogMessage(ex.InnerException.ToString(), string.Empty, TraceLevel.Error); // log exception
        //            }

        //            transaction.Rollback();
        //            this.IsSuccess = false;

        //            VerifySubmitOrderProcess(orderDetailId);

        //            throw;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //    VerifySubmitOrderProcess(orderDetailId);

        //    return order;
        //}
        ///*Nivi Code commented for AWCT- 475 for penny issue.*/
        public override ZnodeOrderFulfillment GetOrderFullfillment(UserAddressModel userAccount, ZnodePortalCart shoppingCart, int portalId)
        {
            //AWCTZnodePortalCart awctshoppingCart = new AWCTZnodePortalCart();
            ////awctshoppingCart.Total = shoppingCart.Total;

            //shoppingCart.AddressCarts.ForEach(x =>
            //{
            //    double SubTotalUpdated = Convert.ToDouble(x.Total.ToString("F"));
            //    decimal totalDiscount = Math.Truncate(x.Discount * 100) / 100;

            //    //string sss = Convert.ToString(totalDiscount);
            //    awctshoppingCart.Total = Convert.ToDecimal(SubTotalUpdated);
            //    awctshoppingCart.Discount = Math.Truncate(totalDiscount * 100) / 100;
                
            //    });


            ZnodeOrderFulfillment order = new ZnodeOrderFulfillment(shoppingCart);
            //ZnodeOrderFulfillment order = new ZnodeOrderFulfillment(awctshoppingCart);

            order.PortalId = portalId;
            SetOrderDetails(order, shoppingCart, userAccount);

            foreach (ZnodeMultipleAddressCart addressCart in shoppingCart.AddressCarts)

            {
                //For Quote
                int? shippingId = 0;
                if (addressCart.Shipping.ShippingID == 0)
                {
                    shippingId = addressCart.Shipping.ShippingID != 0 ? addressCart.Shipping.ShippingID : shoppingCart.Shipping.ShippingID;
                }
                else
                {
                    shippingId = addressCart.Shipping.ShippingID != 0 ? addressCart.Shipping.ShippingID : this._ShippingID;
                }
                AddressModel address = userAccount?.ShippingAddress;
                address = IsNull(address) || Equals(address.AddressId, 0) ? shoppingCart.Payment?.ShippingAddress : address;
                addressCart.OrderShipmentID = CreateOrderShipment(address, shippingId, userAccount?.Email);

                SetOrderLineItems(order, addressCart);
            }
            return order;
        }

        public override void SetOrderShipmentDetails(ZnodeOrderFulfillment order)
        {
            foreach (ZnodeMultipleAddressCart addressCart in ShoppingCart?.AddressCarts)
            {
                //For Quote
                if (order.OrderLineItems[0].OmsOrderShipmentId > 0 && addressCart.OrderShipmentID == 0)
                {
                    addressCart.OrderShipmentID = order.OrderLineItems[0].OmsOrderShipmentId;
                }
                // Check to get the order shipment for the item  
                if (addressCart.OrderShipmentID > 0)
                {
                    foreach (OrderLineItemModel orderLineItemModel in order.OrderLineItems?.Where(w => w.OmsOrderShipmentId == addressCart.OrderShipmentID))
                    {
                        orderHelper.GetOrderShipmentAddress(addressCart.OrderShipmentID, orderLineItemModel);
                    }
                }
            }
        }

        //public override void SetOrderDetails(ZnodeOrderFulfillment order, ZnodePortalCart shoppingCart, UserAddressModel userAccount)
        //{
        //    order.UserID = userAccount.UserId;
        //    order.Created = GetDateTime();
        //    order.Modified = GetDateTime();
        //    order.CreatedBy = userAccount.UserId;
        //    order.ModifiedBy = userAccount.UserId;
        //    order.TaxCost = shoppingCart.TaxCost;
        //    order.VAT = shoppingCart.VAT;
        //    order.SalesTax = shoppingCart.SalesTax;
        //    order.HST = shoppingCart.HST;
        //    order.PST = shoppingCart.PST;
        //    order.GST = shoppingCart.GST;
        //    order.Email = userAccount.Email;
        //    order.CurrencyCode = shoppingCart.CurrencyCode;
        //    order.CultureCode = shoppingCart.CultureCode;
        //    order.ShippingCost = shoppingCart.ShippingCost;
        //    order.ShippingDifference = shoppingCart.ShippingDifference;
        //    order.SubTotal = shoppingCart.SubTotal;
        //    order.Total = shoppingCart.Total;
        //    // order.Total = Convert.ToDouble(Math.Truncate(shoppingCart.Total * 100) / 100);
        //    //double TotalUpdated = 0;
        //    //shoppingCart.AddressCarts.ForEach(x =>
        //    //{
        //    //    //double SubTotalUpdated = Convert.ToDouble(x.Total.ToString("F"));

        //    //    //TotalUpdated = Convert.ToDouble(Math.Truncate(x.Total * 100) / 100);
        //    //    //decimal totalDiscount = Math.Truncate(x.Discount * 100) / 100;

        //    //    //string sss = Convert.ToString(totalDiscount);
        //    //    order.Total = x.Total;
        //    //    //order.DiscountAmount = Math.Truncate(x.Discount * 100) / 100;
        //    //    //order.Discount = Math.Truncate(x.Discount * 100) / 100;
        //    //});


        //    order.ExternalId = shoppingCart.ExternalId;
        //    order.DiscountAmount = shoppingCart.Discount;
        //    order.BillingAddress = userAccount.BillingAddress;
        //    order.ShippingAddress = userAccount.ShippingAddress;
        //    order.OrderDate = shoppingCart.OrderDate.GetValueOrDefault();
        //    order.PaymentTrancationToken = shoppingCart.Token;
        //    order.CreditCardNumber = shoppingCart.CreditCardNumber;
        //    order.CardType = shoppingCart.CardType;
        //    order.CreditCardExpMonth = shoppingCart.CreditCardExpMonth;
        //    order.CreditCardExpYear = shoppingCart.CreditCardExpYear;
        //    order.IsShippingCostEdited = IsNotNull(shoppingCart.CustomShippingCost);
        //    order.IsTaxCostEdited = IsNotNull(shoppingCart.CustomTaxCost);
        //    order.Custom1 = shoppingCart.Custom1;
        //    order.Custom2 = shoppingCart.Custom2;
        //    order.Custom3 = shoppingCart.Custom3;
        //    order.Custom4 = shoppingCart.Custom4;
        //    order.Custom5 = shoppingCart.Custom5;
        //    order.PublishStateId = shoppingCart.PublishStateId;
        //    order.EstimateShippingCost = shoppingCart.EstimateShippingCost;

        //    order.PaymentDisplayName = shoppingCart?.Payment?.PaymentDisplayName;
        //    order.PaymentExternalId = shoppingCart?.Payment?.PaymentExternalId;
        //    foreach (ZnodeCoupon coupon in shoppingCart.Coupons)
        //    {
        //        if (coupon.CouponApplied && coupon.CouponValid)
        //        {
        //            order.CouponCode = (!string.IsNullOrEmpty(order.CouponCode)) ? order.CouponCode += ZnodeConstant.CouponCodeSeparator + coupon.Coupon : coupon.Coupon;
        //        }
        //    }
        //    SetOrderModel(order);
        //}


    }
}
