﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Api.Custom.Service.Service;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.ShoppingCart
{
    public class AWCTZnodeShoppingCart : ZnodeShoppingCart
    {
        private readonly IPublishProductHelper publishProductHelper = GetService<IPublishProductHelper>();
        // Gets total discount of applied to the items in the shopping cart.

        private readonly IZnodeOrderHelper orderHelper;
        private readonly IZnodeRepository<ZnodeOmsCookieMapping> _cookieMappingRepository;

        public AWCTZnodeShoppingCart()
        {
            publishProductHelper = GetService<IPublishProductHelper>();
            orderHelper = GetService<IZnodeOrderHelper>();
            _cookieMappingRepository = new ZnodeRepository<ZnodeOmsCookieMapping>();
            //_orderInventoryManageHelper = GetService<IOrderInventoryManageHelper>();
        }
        public override decimal Discount
        {
            get
            {
                decimal totalDiscount = OrderLevelDiscount + ShoppingCartItems.Cast<ZnodeShoppingCartItem>().Sum(x => x.ExtendedPriceDiscount + x.DiscountAmount);
                /*Nivi Code start for penny issue*/
                //totalDiscount = Math.Truncate(totalDiscount * 100) / 100;
                /*Nivi Code end for penny issue*/
                totalDiscount = totalDiscount + TotalPartialRefundAmount;

                if (totalDiscount > SubTotal)
                {
                    return SubTotal;
                }

                return totalDiscount;
                // return Math.Round(totalDiscount, 2);
            }
        }
        public override decimal Total
        {
            get
            {
                /*Nivi Code start for Penny issue*/
                //double SubTotalUpdated = Convert.ToDouble(SubTotal.ToString("F"));
                //double DiscountUpdated = Convert.ToDouble(Discount.ToString("F"));
                //double ShippingCostUpdated = Convert.ToDouble(ShippingCost.ToString("F"));
                //double ShippingDifferenceUpdated = Convert.ToDouble(ShippingDifference.ToString("F"));
                //double OrderLevelTaxesUpdated = Convert.ToDouble(OrderLevelTaxes.ToString("F"));
                //decimal OrderLevelTaxesUpdated = Convert.ToDecimal(Math.Truncate(OrderLevelTaxes * 100) / 100);
                //double GiftCardAmountUpdated = Convert.ToDouble(GiftCardAmount.ToString("F"));
                //double CSRDiscountUpdated = Convert.ToDouble(CSRDiscount.ToString("F"));
                /*Original Code*/
                decimal total = (SubTotal - Discount) + ShippingCost + ShippingDifference + OrderLevelTaxes - GiftCardAmount - CSRDiscount + GetAdditionalPrice();

                //decimal total = (SubTotal - Discount) + ShippingCost + ShippingDifference + OrderLevelTaxesUpdated - GiftCardAmount - CSRDiscount + GetAdditionalPrice();
                //decimal total = (Convert.ToDecimal(SubTotalUpdated) - Convert.ToDecimal(DiscountUpdated)) + Convert.ToDecimal(ShippingCostUpdated) + Convert.ToDecimal(ShippingDifferenceUpdated) + Convert.ToDecimal(OrderLevelTaxesUpdated) - Convert.ToDecimal(GiftCardAmountUpdated) - Convert.ToDecimal(CSRDiscountUpdated) + GetAdditionalPrice();
                /*Nivi Code end for Penny issue*/

                return total;
                //return Math.Round(total, 2);
            }
        }
        protected override ZnodeProduct GetProductDetailsV2(PublishProductModel publishProduct, int portalId, int localeId, string countryCode = null, bool isGroupProduct = false, string parentSKU = "", int userId = 0, int omsOrderId = 0, string parentSKUProductName = null, int profileId = 0, List<TaxClassRuleModel> lstTaxClassSKUs = null, List<ConfigurableProductEntity> configEntities = null)
        {
            if ((IsNull(publishProduct)))
            {
                return null;
            }

            publishProduct.ParentSEOCode = parentSKU;
            //TODO
            if (IsNull(publishProduct.RetailPrice))
            {
                GetParentProductPriceDetails(publishProduct, portalId, localeId, parentSKU, userId, profileId);
            }
            string style = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "awctstyle")?.AttributeValues;
            publishProduct.Name = publishProduct.Name.Contains(style) ? publishProduct.Name : style + " " + publishProduct.Name;

            List<AttributesSelectValuesModel> inventorySetting = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.Vendor)?.SelectValues;
            string vendorCode = inventorySetting?.Count > 0 ? inventorySetting.FirstOrDefault().Code : string.Empty;

            ZnodeProduct product = new ZnodeProduct
            {
                ProductID = publishProduct.PublishProductId,
                SEOURL = publishProduct.SEOUrl,
                Name = isGroupProduct ? parentSKUProductName : publishProduct.Name,
                SKU = isGroupProduct ? parentSKU : publishProduct.SKU,
                SalePrice = publishProduct.SalesPrice,
                RetailPrice = publishProduct.RetailPrice.GetValueOrDefault(),
                QuantityOnHand = publishProduct.Quantity.GetValueOrDefault(),
                ZNodeTieredPriceCollection = GetZnodeProductTierPrice(publishProduct),
                TaxClassID = GetTaxClassBySKU(publishProduct.SKU, countryCode, lstTaxClassSKUs),
                AddOns = publishProduct.AddOns,
                IsPriceExist = isGroupProduct ? true : IsProductPriceExist(publishProduct.SalesPrice, publishProduct.RetailPrice),
                VendorCode = vendorCode,
                IsActive = publishProduct.IsActive,
                ProductCategoryIds = new int[] { publishProduct.ZnodeCategoryIds },
                AllowedTerritories = GetProductAttributeAllowedTerritoriesValue(publishProduct, ZnodeConstant.AllowedTerritories)
            };
            if (publishProduct.Attributes?.Count > 0)
            {
                product.AllowBackOrder = GetBooleanProductAttributeValue(publishProduct, ZnodeConstant.AllowBackOrdering);
                product.FreeShippingInd = GetBooleanProductAttributeValue(publishProduct, ZnodeConstant.FreeShipping);
                product.ShipSeparately = GetBooleanProductAttributeValue(publishProduct, ZnodeConstant.ShipSeparately);
                product.MinQty = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.MinimumQuantity);
                product.MaxQty = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.MaximumQuantity);
                product.InventoryTracking = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues?.FirstOrDefault()?.Code ?? string.Empty;
                product.ShippingRuleTypeCode = publishProduct.Attributes?.Where(x => x.AttributeCode == ZnodeConstant.ShippingCost)?.FirstOrDefault()?.SelectValues?.FirstOrDefault()?.Code;
                product.BrandCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.Brand)?.SelectValues?.FirstOrDefault()?.Code ?? string.Empty;
                product.Height = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Height);
                product.Width = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Width);
                product.Length = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Length);
                product.Weight = GetDecimalProductAttributeValue(publishProduct, ZnodeConstant.Weight);
                product.UOM = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.UOM)?.SelectValues?.FirstOrDefault()?.Value;
                product.Container = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ShippingContainer)?.SelectValues[0]?.Value;
                product.Size = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ShippingSize)?.SelectValues[0]?.Code;
                product.PackagingType = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.PackagingType)?.SelectValues[0]?.Value;
            }

            //to apply product promotional price
            product.ApplyPromotion();
            //set configurable product attributes
            if (publishProduct?.ParentPublishProductId > 0)
            {
                List<ConfigurableProductEntity> configEntiy = configEntities?.Where(x => x.ZnodeProductId == publishProduct.ParentPublishProductId).ToList();

                if (IsNotNull(configEntiy) && configEntiy?.Count > 0)
                {
                    string SeasonName = "Collection" + " - " + publishProduct?.Attributes?.FirstOrDefault(x => x.AttributeCode == "Season")?.SelectValues[0]?.Value;
                    ;
                    product.Description = string.IsNullOrEmpty(product.Description)
                                          ? (configEntiy?.Count > 0
                                          ? SeasonName
                                           + "<br>" + string.Join("<br>", publishProduct?.Attributes?.Where(x => x.IsConfigurable && (configEntiy?.FirstOrDefault()?.ConfigurableAttributeCodes?.Contains(x.AttributeCode)).GetValueOrDefault()).OrderBy(x => x.AttributeCode).Select(x => x.AttributeName + " - " + x.SelectValues?.FirstOrDefault()?.Value).Distinct())
                                          : string.Join("<br>", publishProduct?.Attributes?.Where(x => x.IsConfigurable)?.Select(x => x.AttributeName + " - " + x.SelectValues?.FirstOrDefault()?.Value)?.Distinct()))
                                          : product.Description;
                }
                else
                {
                    product.Description = string.IsNullOrEmpty(product.Description) ? string.Join("<br>", publishProduct?.Attributes?.Where(x => x.IsConfigurable)?.Select(x => x.AttributeName + " - " + x.SelectValues?.FirstOrDefault()?.Value)?.Distinct()) : product.Description; ;
                }

            }
            //to set product attributes 
            SetProductAttributes(product, publishProduct);
            return product;
        }

        private int GetTaxClassBySKU(string sku, string countryCode, List<TaxClassRuleModel> lstTaxClassSKUs)
        {
            int? taxClassId = lstTaxClassSKUs?.Where(x => x.SKU == sku && (x.DestinationCountryCode == countryCode || x.DestinationCountryCode == null))?.FirstOrDefault()?.TaxClassId;

            return taxClassId ?? 0;
        }
        protected override void BindCartProductDetails(ShoppingCartItemModel cartModel, int publishCatalogId, int localeId, string groupIdProductAttribute = "", GlobalSettingValues groupIdPersonalizeAttribute = null)
        {
            int catalogVersionId = GetCatalogVersionId(publishCatalogId);
            ProductEntity product = publishProductHelper.GetPublishProductBySKU(string.IsNullOrEmpty(cartModel.ConfigurableProductSKUs) ? cartModel.SKU : cartModel.ConfigurableProductSKUs, publishCatalogId, localeId, catalogVersionId);
            if (IsNotNull(product))
            {
                string StyleName = product.Attributes.FirstOrDefault(y => y.AttributeCode == "awctstyle")?.AttributeValues;
                cartModel.Description = GetShortDescription(product, cartModel);
                cartModel.ProductName = cartModel.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group) ? cartModel.ProductName : StyleName + " " + product.Name;
                cartModel.GroupId = GenerateGroupId(cartModel, product, groupIdProductAttribute, groupIdPersonalizeAttribute);
            }
        }

        /*Nivi Code start for guard quote*/
        public override ZnodeShoppingCart LoadFromDatabase(CartParameterModel cartParameterModel)
        {
            cartParameterModel.CookieId = !string.IsNullOrEmpty(cartParameterModel.CookieMappingId) ? Convert.ToInt32(new ZnodeEncryption().DecryptData(cartParameterModel.CookieMappingId)) : 0;

            //AccountQuoteLineItemModel contains properties of Account Quotes and Saved cart line items.
            List<AccountQuoteLineItemModel> cartLineItems;
            /*Nivi Code start for guard quote*/

            if (cartParameterModel.OmsOrderId > 0)
            {
                cartLineItems = GetAccountQuoteLineItems(cartParameterModel);

            }
            else if (cartParameterModel.OmsQuoteId > 0)
            {
                cartLineItems = GetQuoteLineItemsById(cartParameterModel);
                cartLineItems.ForEach(x =>
                {
                    string[] Productname = x.ProductName.Split('|');
                    x.ProductName = Productname[0];
                    x.Custom1 = Productname[1];
                    x.Custom2 = Productname[2];
                    x.Custom3 = Productname[3];
                    x.Custom4 = Productname[4];
                    x.Custom5 = Productname[5];

                });

            }
            /*Nivi Code start for guard quote*/
            else
            {
                //Check if cookieMappingId is null or 0.
                if (string.IsNullOrEmpty(cartParameterModel.CookieMappingId) || cartParameterModel.CookieId == 0)
                {
                    List<ZnodeOmsCookieMapping> cookieMappings = orderHelper.GetCookieMappingList(cartParameterModel);
                    cartParameterModel.CookieId = Convert.ToInt32(cookieMappings?.FirstOrDefault()?.OmsCookieMappingId);
                    cartParameterModel.CookieMappingId = new ZnodeEncryption().EncryptData(cartParameterModel.CookieId.ToString());
                }

                //Get saved cart line items.
                cartLineItems = GetSavedCartLineItems(cartParameterModel);
            }

            List<string> skus = cartLineItems.Select(x => x.SKU.ToLower())?.Distinct().ToList();

            List<string> navigationProperties = new List<string> { ZnodeConstant.Promotions, ZnodeConstant.Pricing, ZnodeConstant.Inventory, ZnodeConstant.AddOns, ZnodeConstant.SEO };

            int catalogVersionId = publishProductHelper.GetCatalogVersionId(cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId);

            List<PublishProductModel> cartLineItemsProductData = publishProductHelper.GetDataForCartLineItems(skus, cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId, navigationProperties, cartParameterModel.UserId.GetValueOrDefault(), cartParameterModel.PortalId, catalogVersionId, out List<ConfigurableProductEntity> configEntities);

            List<TaxClassRuleModel> lstTaxClassSKUs = GetTaxRules(skus);

            List<ZnodePimDownloadableProduct> lstDownloadableProducts = new ZnodeRepository<ZnodePimDownloadableProduct>().Table.Where(x => skus.Contains(x.SKU)).ToList();


            //Set Portal Id in Context Header, to avoid loop based calls.
            SetPortalIdInRequestHeader();

            //Get the Saved Cart Line Item ids, to avoid loop based calls.
            List<int?> lstCartLineIds = GetSavedCartLineItemIds(cartLineItems);

            List<PersonaliseValueModel> lstPersonlizedValues = GetService<IZnodeOrderHelper>()?.GetPersonalisedValueCartLineItem(lstCartLineIds);
            //clear existing items in shopping cart           
            foreach (AccountQuoteLineItemModel cartLineItem in cartLineItems)
            {
                bool isConfigurableExists = false;
                if (cartParameterModel.OmsQuoteId > 0)
                {
                    isConfigurableExists = ((cartLineItem.ParentOmsQuoteLineItemId < 1 || cartLineItem.ParentOmsQuoteLineItemId == null) && cartLineItems.Any(lineItem => lineItem?.ParentOmsQuoteLineItemId == cartLineItem.OmsQuoteLineItemId && (lineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles))))
                       ? true
                       : cartLineItems.Any(lineItem => lineItem?.OmsQuoteLineItemId == cartLineItem.ParentOmsQuoteLineItemId && ((cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Configurable)) || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)) || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Simple))));
                }
                else
                {
                    isConfigurableExists = (cartLineItem.ParentOmsSavedCartLineItemId < 1 && cartLineItems.Any(lineItem => lineItem?.ParentOmsSavedCartLineItemId == cartLineItem.OmsSavedCartLineItemId && ((lineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)))))
                   ? true
                   : cartLineItems.Any(lineItem => lineItem?.OmsSavedCartLineItemId == cartLineItem.ParentOmsSavedCartLineItemId
                   && ((cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Configurable))
                   || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Simple))
                   || (cartLineItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group))
                   ));
                }

                //add new items from savedcartlineitem in shopping cart                              
                if (isConfigurableExists)
                {
                    this.AddToShoppingCartV2(cartLineItem, cartLineItems, cartParameterModel, cartLineItemsProductData, catalogVersionId, lstTaxClassSKUs, lstDownloadableProducts, configEntities, lstPersonlizedValues);
                }
            }
            return this;
        }
        private void SetPortalIdInRequestHeader()
        {
            //if site config is null then get the portal id from domain
            int portalId = ZnodeConfigManager.SiteConfig != null ? ZnodeConfigManager.SiteConfig.PortalId : GetPortalId();
            if (!Equals(HttpContext.Current, null) && !Equals(HttpContext.Current.Request, null) && !Equals(HttpContext.Current.Request.Headers, null))
            {
                HttpContext.Current.Request.Headers.Add("Znode-Cart-PortalId", portalId.ToString());
            }
        }
        private List<int?> GetSavedCartLineItemIds(List<AccountQuoteLineItemModel> cartLineItems)
        {
            List<int?> lstDDD = new List<int?>();

            cartLineItems?.ForEach(item =>
            {
                //This will have only child cart line item id. For simple product it will have the parent ID itself.
                lstDDD.Add(item.OmsSavedCartLineItemId);
            });
            return lstDDD;
        }

        public virtual List<AccountQuoteLineItemModel> GetQuoteLineItemsById(CartParameterModel cartParameterModel)
        {

            // ZnodeOmsQuote quoteDetails = new ZnodeOmsQuote();
            List<AccountQuoteLineItemModel> cartLineItems = new List<AccountQuoteLineItemModel>();

            try
            {
                if (cartParameterModel.OmsQuoteId > 0)
                {
                    int RowCount = 0;
                    IZnodeViewRepository<ZnodeOmsQuoteLineItem> objStoredProc = new ZnodeViewRepository<ZnodeOmsQuoteLineItem>();
                    objStoredProc.SetParameter("OmsQuoteId", cartParameterModel.OmsQuoteId, ParameterDirection.Input, DbType.Int32);
                    objStoredProc.SetParameter("RowCount", RowCount, ParameterDirection.Output, DbType.Int32);
                    cartLineItems = objStoredProc.ExecuteStoredProcedureList("AWCT_GetQuoteLineItemsbyId @OmsQuoteId,@RowCount OUT", 1, out RowCount)?.ToModel<AccountQuoteLineItemModel>()?.ToList();
                }
                return cartLineItems;

            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return cartLineItems;
            }
        }
    }
}
