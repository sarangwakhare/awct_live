﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Custom.Cache.Cache
{
    public class QuoteCache : BaseCache, IQuoteCache
    {
        private readonly IQuoteService _quoteService;

        public QuoteCache(IQuoteService quoteService)
        {
            this._quoteService = quoteService;
        }

        public virtual string GetQuoteList(string routeUri, string routeTemplate)
        {
            string str = this.GetFromCache(routeUri);
            if (string.IsNullOrEmpty(str))
            {
                QuoteListModel quoteList = this._quoteService.GetQuoteList(this.Filters, this.Sorts, this.Page);
                QuoteListResponse listResponse = new QuoteListResponse()
                {
                    QuoteList = quoteList,
                    PortalName = quoteList?.PortalName
                };
                if (quoteList != null)
                {
                    int? count = quoteList.Quotes?.Count;
                    int num = 0;
                    if (count.GetValueOrDefault() > num & count.HasValue)
                        listResponse.MapPagingDataFromModel(quoteList);
                }
                str = this.InsertIntoCache(routeUri, routeTemplate, (object)listResponse);
            }
            return str;
        }

        public virtual string GetQuoteReceipt(int quoteId, string routeUri, string routeTemplate)
        {
            string str = this.GetFromCache(routeUri);
            if (string.IsNullOrEmpty(str))
            {
                QuoteResponseModel quoteReceipt = this._quoteService.GetQuoteReceipt(quoteId);
                if (HelperUtility.IsNotNull((object)quoteReceipt))
                {
                    QuoteResponse quoteResponse = new QuoteResponse()
                    {
                        QuoteDetails = quoteReceipt
                    };
                    str = this.InsertIntoCache(routeUri, routeTemplate, (object)quoteResponse);
                }
            }
            return str;
        }

        public virtual string GetGuestQuoteReceipt(QuoteModel quoteModel, string routeUri, string routeTemplate)
        {
            string str = this.GetFromCache(routeUri);
            if (string.IsNullOrEmpty(str))
            {
                QuoteResponseModel quoteReceipt = this._quoteService.GetGuestQuoteReceipt(quoteModel);
                if (HelperUtility.IsNotNull((object)quoteReceipt))
                {
                    QuoteResponse quoteResponse = new QuoteResponse()
                    {
                        QuoteDetails = quoteReceipt
                    };
                    str = this.InsertIntoCache(routeUri, routeTemplate, (object)quoteResponse);
                }
            }
            return str;
        }


        public virtual string GetQuoteById(int omsQuoteId, string routeUri, string routeTemplate)
        {
            string str = this.GetFromCache(routeUri);
            if (string.IsNullOrEmpty(str))
            {
                QuoteResponseModel quoteById = this._quoteService.GetQuoteById(omsQuoteId);
                if (HelperUtility.IsNotNull((object)quoteById))
                {
                    QuoteDetailResponse quoteDetailResponse = new QuoteDetailResponse()
                    {
                        QuoteDetail = quoteById
                    };
                    str = this.InsertIntoCache(routeUri, routeTemplate, (object)quoteDetailResponse);
                }
            }
            return str;
        }

        public virtual string GetQuoteLineItems(int omsQuoteId, string routeUri, string routeTemplate)
        {
            string str = this.GetFromCache(routeUri);
            if (string.IsNullOrEmpty(str))
            {
                List<QuoteLineItemModel> quoteLineItems = this._quoteService.GetQuoteLineItems(omsQuoteId);
                QuoteLineItemResponse lineItemResponse = new QuoteLineItemResponse()
                {
                    QuoteLineItems = quoteLineItems
                };
                str = this.InsertIntoCache(routeUri, routeTemplate, (object)lineItemResponse);
            }
            return str;
        }
    }
}
