﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Sample.Api.Model.Quote;

namespace Znode.Api.Custom.IHelper
{
    public interface IPaymentHelper
    {
        /// <summary>
        /// Process Payment
        /// </summary>
        /// <param name="convertToOrderModel"></param>
        /// <param name="cartModel"></param>
        /// <returns></returns>
        bool ProcessPayment(ConvertQuoteToOrderModel convertToOrderModel, ShoppingCartModel cartModel);
    }
}
