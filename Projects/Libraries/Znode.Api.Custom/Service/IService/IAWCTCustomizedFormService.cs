﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Services;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using System.Web;
using Znode.Sample.Api.Model.CustomizedFormModel;

namespace Znode.Api.Custom.Service.IService
{
    public interface IAWCTCustomizedFormService 
    {

        bool ModelSearch(AWCTModelSearchModel model);

        bool NewCustomerApplication(AWCTNewCustomerApplicationModel model);
        bool BecomeAContributer(AWCTBecomeAContributerModel model);
        bool PreviewShow(AWCTPreviewShowModel model);
        /*Start Quote Lookup*/
        bool QuoteLookup(AWCTQuoteLookupModel model);
        /*End Quote Lookup*/
    }
}
