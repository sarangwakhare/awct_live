﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTEmailTemplateService : EmailTemplateService
    {
        #region Private variables              
        private readonly IZnodeRepository<ZnodeEmailTemplate> _emailTemplateRepository;
        private readonly IZnodeRepository<ZnodeEmailTemplateLocale> _emailTemplateLocaleRepository;
        private readonly IZnodeRepository<ZnodeEmailTemplateMapper> _emailTemplateMapperRepository;
        private readonly IZnodeRepository<ZnodeEmailTemplateArea> _emailTemplateAreaRepository;
        public const string ResetPassword = "ResetPassword";
        private readonly IZnodeRepository<ZnodeDomain> _domainRepository;
        #endregion

        public AWCTEmailTemplateService()
        {
            _emailTemplateRepository = new ZnodeRepository<ZnodeEmailTemplate>();
            _emailTemplateLocaleRepository = new ZnodeRepository<ZnodeEmailTemplateLocale>();
            _emailTemplateMapperRepository = new ZnodeRepository<ZnodeEmailTemplateMapper>();
            _emailTemplateAreaRepository = new ZnodeRepository<ZnodeEmailTemplateArea>();
            _domainRepository = new ZnodeRepository<ZnodeDomain>();
        }
        public override EmailTemplateModel CreateTemplatePage(EmailTemplateModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            if (Equals(model, null))
                throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorEmailTemplateModelNull);

            ZnodeLogging.LogMessage("Input parameter EmailTemplateModel: ", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, model);

            //Check if template name already exists.
            if (_emailTemplateRepository.Table.Count(x => x.TemplateName.Trim() == model.TemplateName.Trim()) > 0)
                throw new ZnodeException(ErrorCodes.AlreadyExist, Admin_Resources.ErrorEmailTemplateAlreadyExists);

            //If LocaleId is zero then get default locale.
            if (model.LocaleId == 0)
                model.LocaleId = GetDefaultLocaleId();

            model.Html = string.Format(Admin_Resources.FormatEmailTemplateBody, model.Html);
            //Insert into Email Template.
            EmailTemplateModel emailTemplateModel = _emailTemplateRepository.Insert(model.ToEntity<ZnodeEmailTemplate>()).ToModel<EmailTemplateModel>();

            //Insert into Email Template Locale.
            if (emailTemplateModel?.EmailTemplateId > 0)
            {
                model.EmailTemplateId = emailTemplateModel.EmailTemplateId;

                ZnodeLogging.LogMessage(string.Format(Admin_Resources.SuccessEmailTemplateCreatedWithId, emailTemplateModel.EmailTemplateId), ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

                ZnodeLogging.LogMessage(_emailTemplateLocaleRepository.Insert(model?.ToEntity<ZnodeEmailTemplateLocale>())?.EmailTemplateLocaleId > 0
                   ? Admin_Resources.SuccessEmailTemplateLocaleInserted : Admin_Resources.ErrorInsertEmailTemplateLocale, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);


                EmailTemplateAreaModel emailTemplateAreaModel = new EmailTemplateAreaModel();
                emailTemplateAreaModel.Code = emailTemplateModel.TemplateName;
                emailTemplateAreaModel.Name = emailTemplateModel.TemplateName;
                emailTemplateAreaModel.CreatedBy = emailTemplateModel.CreatedBy;
                emailTemplateAreaModel.CreatedDate = emailTemplateModel.CreatedDate;
                emailTemplateAreaModel.ModifiedBy = emailTemplateModel.ModifiedBy;
                emailTemplateAreaModel.ModifiedDate = emailTemplateModel.ModifiedDate;               
              
		
		        /* Nivi Changes start*/
                EmailTemplateAreaModel EmailTemplateAreaModel = _emailTemplateAreaRepository.Insert(emailTemplateAreaModel.ToEntity<ZnodeEmailTemplateArea>()).ToModel<EmailTemplateAreaModel>();

                List<ZnodeDomain> domain = _domainRepository.Table.Where(x => Equals(x.ApplicationType, "Webstore")).ToList();

                int PortalId = domain.Where(x => Equals(x.IsDefault, Convert.ToBoolean(0))).Select(x => x.PortalId).FirstOrDefault();
                _emailTemplateMapperRepository.Insert(new ZnodeEmailTemplateMapper
                {
                    EmailTemplateId = emailTemplateModel.EmailTemplateId,
                    EmailTemplateAreasId = EmailTemplateAreaModel.EmailTemplateAreasId,
                    ModifiedDate = emailTemplateModel.ModifiedDate,
                    CreatedDate = emailTemplateModel.CreatedDate,
                    IsEnableBcc = false,
                    ModifiedBy = emailTemplateModel.ModifiedBy,
                    CreatedBy = emailTemplateModel.CreatedBy,
                    PortalId = EmailTemplateAreaModel.Name.Contains("Guard") ? PortalId : GetPortalId(),
                    IsActive = true

                });
                
		        //ZnodeLogging.LogMessage(_emailTemplateMapperRepository.Insert(emailTemplateMapperModel?.ToEntity<ZnodeEmailTemplateMapper>())?.EmailTemplateMapperId > 0
                //   ? Admin_Resources.SuccessEmailTemplateLocaleInserted : Admin_Resources.ErrorInsertEmailTemplateLocale, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
		
		        /* Nivi Changes end*/

                return model;
            }
            ZnodeLogging.LogMessage(Admin_Resources.ErrorEmailTemplateCreate, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            return model;
        }

        public static int GetPortalId()
        {
            string domainName = HelperUtility.GetPortalDomainName();
            return ZnodeConfigManager.GetSiteConfig(domainName)?.PortalId ?? 0;
        }
    }
}
