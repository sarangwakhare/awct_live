﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Maps;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
   public class AWCTGlobalAttributeService : GlobalAttributeService
    {


        #region Private Variables
 
        private readonly IZnodeRepository<ZnodeGlobalAttributeValidation> _validationRepository;
        private readonly IZnodeRepository<ZnodeGlobalAttributeDefaultValue> _defaultValueRepository;
        #endregion
        #region Public Constructor
        public AWCTGlobalAttributeService()
        {
           
            _validationRepository = new ZnodeRepository<ZnodeGlobalAttributeValidation>();
            _defaultValueRepository = new ZnodeRepository<ZnodeGlobalAttributeDefaultValue>();
            
        }
        #endregion
        
        public override GlobalAttributeDefaultValueListModel GetDefaultValues(int attributeId)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameter attributeId: ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, attributeId);

            _validationRepository.EnableDisableLazyLoading = true;

            GlobalAttributeDefaultValueListModel listModel;
            listModel = GlobalAttributeMap.ToDefaultValueListModel(_defaultValueRepository.GetEntityList(GetWhereClauseForAttributeId(attributeId).WhereClause));
            ZnodeLogging.LogMessage("GlobalAttributeDefaultValueListModel list count: ", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Verbose, listModel?.DefaultValues?.Count);
            /*Nivi Code start*/
            listModel.DefaultValues = listModel.DefaultValues.OrderBy(x => x.DisplayOrder).ToList();
            /*Nivi Code end*/
            foreach (var item in listModel.DefaultValues)
            {
                item.MediaPath = item.MediaId > 0 ? GetMediaPath(item) : string.Empty;
            }
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Admin.ToString(), TraceLevel.Info);
            return listModel;
        }
    }
}
