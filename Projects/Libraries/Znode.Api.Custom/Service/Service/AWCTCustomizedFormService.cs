﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.CustomizedFormModel;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTCustomizedFormService : BaseService, IAWCTCustomizedFormService
    {

        public virtual bool ModelSearch(AWCTModelSearchModel model)
        {
            string senderEmail = GetServiceMail(ZnodeConfigManager.SiteConfig.PortalId); ;
            string receiverEmail = senderEmail;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string subject = ""; 
                //$"{senderEmail.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ModelSearch", (PortalId > 0) ? PortalId : PortalId, 1);
            
            if (HelperUtility.IsNull(emailTemplateMapperModel))
            {
                throw (new Exception("Not Implemented"));
            }
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);
                subject = emailTemplateMapperModel?.Subject;
                string messageText = GetEmailTemplate(model, receiptHelper);
                //List<string> attachment = new List<string>();
                //attachment.Add("C:\\Users\\pratik.badwaik\\Desktop\\issues\\" + model.AdditionalPhotoUpload );
                //attachment.Add("C:\\Users\\pratik.badwaik\\Desktop\\issues\\" + model.HeadShotPhotoUpload );
                //attachment.Add("C:\\Users\\pratik.badwaik\\Desktop\\issues\\" + model.FullLengthPhotoUpload);
                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailAddress, true);
                //return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailAddress,true, attachment);
            }
        }


        private static bool SendMail(string senderEmail, string receiverEmail, string subject, string messageText, bool isEnableBcc, string EmailAddress, bool isHTMLBody, List<string> attachment)
        {
            try
            {

                ZnodeEmail.SendEmail(senderEmail, receiverEmail, subject, messageText, "", "", attachment, true, "", System.Net.Mail.MailPriority.Normal);


                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                return false;
            }
        }


        private string GetEmailTemplate(AWCTModelSearchModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;


            Regex rx1 = new Regex("#ModelName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ModelName != null ? model.ModelName : string.Empty);

            rx1 = new Regex("#ContactName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ContactName != null ? model.ContactName : string.Empty);

            rx1 = new Regex("#Address#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Address != null ? model.Address : string.Empty);

            rx1 = new Regex("#City#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.City != null ? model.City : string.Empty);

            rx1 = new Regex("#StateProvince#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.StateProvince != null ? model.StateProvince : string.Empty);

            rx1 = new Regex("#ZipCode#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ZipCode != null ? model.ZipCode : string.Empty);

            rx1 = new Regex("#DayPhone#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.DayPhone != null ? model.DayPhone : string.Empty);

            rx1 = new Regex("#EveningPhone#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EveningPhone != null ? model.EveningPhone : string.Empty);

            rx1 = new Regex("#CellPhone#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.CellPhone != null ? model.CellPhone : string.Empty);

            rx1 = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EmailAddress != null ? model.EmailAddress : string.Empty);

            rx1 = new Regex("#ArmLength#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ArmLength != null ? model.ArmLength : string.Empty);

            rx1 = new Regex("#ChestMeasurement#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ChestMeasurement != null ? model.ChestMeasurement : string.Empty);

            rx1 = new Regex("#GirthMeasurement#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.GirthMeasurement != null ? model.GirthMeasurement : string.Empty);

            rx1 = new Regex("#HipMeasurement#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.HipMeasurement != null ? model.HipMeasurement : string.Empty);

            rx1 = new Regex("#InseamMeasurement#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.InseamMeasurement != null ? model.InseamMeasurement : string.Empty);

            rx1 = new Regex("#WaistMeasurement#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.WaistMeasurement != null ? model.WaistMeasurement : string.Empty);

            rx1 = new Regex("#Age#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Age != null ? model.Age : string.Empty);

            rx1 = new Regex("#HairColor#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.HairColor != null ? model.HairColor : string.Empty);

            rx1 = new Regex("#EyeColor#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EyeColor != null ? model.EyeColor : string.Empty);

            rx1 = new Regex("#Height#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Height != null ? model.Height : string.Empty);

            rx1 = new Regex("#Weight#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Weight != null ? model.Weight : string.Empty);

            rx1 = new Regex("#LeotardSize#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LeotardSize != null ? model.LeotardSize : string.Empty);

            rx1 = new Regex("#ShoeSize#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ShoeSize != null ? model.ShoeSize : string.Empty);

            rx1 = new Regex("#DanceStudioYouAttend#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.DanceStudioYouAttend != null ? model.DanceStudioYouAttend : string.Empty);

            rx1 = new Regex("#AdditionalPhotoUpload#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.AdditionalPhotoUpload != null ? model.AdditionalPhotoUpload : string.Empty);

            rx1 = new Regex("#HeadShotPhotoUpload#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.HeadShotPhotoUpload != null ? model.HeadShotPhotoUpload : string.Empty);

            rx1 = new Regex("#FullLengthPhotoUpload#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FullLengthPhotoUpload != null ? model.FullLengthPhotoUpload : string.Empty);

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }



        public virtual bool NewCustomerApplication(AWCTNewCustomerApplicationModel model)
        {
            //PortalModel Portalmodel = GetCustomPortalDetails(ZnodeConfigManager.SiteConfig.PortalId);
            string senderEmail = GetServiceMail(ZnodeConfigManager.SiteConfig.PortalId);
            string receiverEmail = senderEmail;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string subject = "";
                //$"{model.EmailAddress.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";

            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("NewCustomerApplicationEmail", (PortalId > 0) ? PortalId : PortalId, 2);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
            {
                throw (new Exception("Not Implemented"));
            }
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);
                subject = emailTemplateMapperModel?.Subject;
                string messageText = GetEmailTemplate(model, receiptHelper);
                string path;
                path = HttpContext.Current.Server.MapPath("~/Data/Attachment");
                string attach = model.StudioAffiliationFile;
                string fileName = Path.Combine(path, attach);
                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailAddress, true);
            }

        }



        private string GetEmailTemplate(AWCTNewCustomerApplicationModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            Regex rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            rx1 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FirstName != null ? model.FirstName : string.Empty);

            rx1 = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LastName != null ? model.LastName : string.Empty);

            rx1 = new Regex("#StudioName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.StudioName != null ? model.StudioName : string.Empty);

            rx1 = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EmailAddress != null ? model.EmailAddress : string.Empty);

            rx1 = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.PhoneNumber != null ? model.PhoneNumber : string.Empty);

            rx1 = new Regex("#CellPhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.CellPhoneNumber != null ? model.CellPhoneNumber : string.Empty);

            rx1 = new Regex("#MailingAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.MailingAddress != null ? model.MailingAddress : string.Empty);

            rx1 = new Regex("#DeliveryAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.DeliveryAddress != null ? model.DeliveryAddress : string.Empty);

            rx1 = new Regex("#WebsiteAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.WebsiteAddress != null ? model.WebsiteAddress : string.Empty);

            //rx1 = new Regex("#ReadyToPlaceOrder#", RegexOptions.IgnoreCase);
            //messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ReadyToPlaceOrder);

            rx1 = new Regex("#StudioAffiliationFile#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.StudioAffiliationFile != null ? model.StudioAffiliationFile : string.Empty);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }


        public virtual bool BecomeAContributer(AWCTBecomeAContributerModel model)
        {
            // PortalModel Portalmodel = GetCustomPortalDetails(ZnodeConfigManager.SiteConfig.PortalId);
            string senderEmail = GetServiceMail(ZnodeConfigManager.SiteConfig.PortalId);
            string receiverEmail = senderEmail;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string subject = $"{model.EmailAddress.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("BecomeAContributer", (PortalId > 0) ? PortalId : PortalId, 2);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
            {
                throw (new Exception("Not Implemented"));
            }
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);

                string messageText = GetEmailTemplate(model, receiptHelper);

                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailAddress, true);

            }
        }

        private static bool SendMail(string senderEmail, string receiverEmail, string subject, string messageText, bool isEnableBcc, string EmailAddress, bool isHTMLBody)
        {
            try
            {
                //SendEmail(string to, string from, string bcc, string subject, string body, bool isBodyHtml, string attachedPath = "", string cc = "");
                // ZnodeEmail.SendEmail(senderEmail, receiverEmail, "", subject, messageText, true, "", EmailAddress);
                ZnodeEmail.SendEmail(senderEmail, receiverEmail, "", subject, messageText, true, "", EmailAddress);
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Search.ToString(), TraceLevel.Error);
                return false;
            }
        }

        private string GetEmailTemplate(AWCTBecomeAContributerModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            Regex rx1 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FirstName != null ? model.FirstName : string.Empty);

            rx1 = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LastName != null ? model.LastName : string.Empty);

            rx1 = new Regex("#Address#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Address != null ? model.Address : string.Empty);

            rx1 = new Regex("#SchooName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.SchooName != null ? model.SchooName : string.Empty);

            rx1 = new Regex("#StateProvince#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.StateProvince != null ? model.StateProvince : string.Empty);

            rx1 = new Regex("#ZipCode#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ZipCode != null ? model.ZipCode : string.Empty);

            rx1 = new Regex("#AlternateNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.AlternateNumber != null ? model.AlternateNumber : string.Empty);

            rx1 = new Regex("#CellPhone#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.CellPhone != null ? model.CellPhone : string.Empty);

            rx1 = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EmailAddress != null ? model.EmailAddress : string.Empty);

            rx1 = new Regex("#CityName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.CityName != null ? model.CityName : string.Empty);

            rx1 = new Regex("#Website#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Website != null ? model.Website : string.Empty);

            rx1 = new Regex("#BlogContributerAppl#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.BlogContributerAppl != null ? model.BlogContributerAppl : string.Empty);

            rx1 = new Regex("#DanceHistory#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.DanceHistory != null ? model.DanceHistory : string.Empty);

            rx1 = new Regex("#UniquePrespective#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.UniquePrespective != null ? model.UniquePrespective : string.Empty);

            rx1 = new Regex("#YourExpert#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.YourExpert != null ? model.YourExpert : string.Empty);

            rx1 = new Regex("#BlogPost#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.BlogPost != null ? model.BlogPost : string.Empty);

            rx1 = new Regex("#FavoriteThing#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FavoriteThing != null ? model.FavoriteThing : string.Empty);

            rx1 = new Regex("#SocialMediaScale#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.SocialMediaScale != null ? model.SocialMediaScale : string.Empty);

            rx1 = new Regex("#Relevant#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Relevant != null ? model.Relevant : string.Empty);

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }

        public virtual bool PreviewShow(AWCTPreviewShowModel model)
        {
            //PortalModel Portalmodel = GetCustomPortalDetails(ZnodeConfigManager.SiteConfig.PortalId);
            //string senderEmail = Portalmodel?.AdministratorEmail;
            //string receiverEmail = Portalmodel?.AdministratorEmail;
            string senderEmail = GetServiceMail(ZnodeConfigManager.SiteConfig.PortalId);
            string receiverEmail = senderEmail;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string subject = ""; 
                //$"{model.EmailAddress.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";

            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("PreviewShow", (PortalId > 0) ? PortalId : PortalId, 1);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
            {
                throw (new Exception("Not Implemented"));
            }
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);
                subject = emailTemplateMapperModel?.Subject;
                string messageText = GetEmailTemplate(model, receiptHelper);

                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.EmailAddress, true);

            }

        }

        private string GetEmailTemplate(AWCTPreviewShowModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            Regex rx1 = new Regex("#FirstName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.FirstName != null ? model.FirstName : string.Empty);

            rx1 = new Regex("#LastName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.LastName != null ? model.LastName : string.Empty);

            rx1 = new Regex("#StudioName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.StudioName != null ? model.StudioName : string.Empty);

            rx1 = new Regex("#CustomerNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.CustomerNumber != null ? model.CustomerNumber : string.Empty);

            rx1 = new Regex("#Address#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.Address != null ? model.Address : string.Empty);

            rx1 = new Regex("#City#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.City != null ? model.City : string.Empty);

            rx1 = new Regex("#State#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.State != null ? model.State : string.Empty);

            rx1 = new Regex("#ZipCode#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.ZipCode != null ? model.ZipCode : string.Empty);

            rx1 = new Regex("#PhoneNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.PhoneNumber != null ? model.PhoneNumber : string.Empty);

            rx1 = new Regex("#EmailAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.EmailAddress != null ? model.EmailAddress : string.Empty);

            rx1 = new Regex("#NoOfTeacher#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.NoOfTeacher != null ? model.NoOfTeacher : string.Empty);

            rx1 = new Regex("#AnotherDay#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.AnotherDay != null ? model.AnotherDay : string.Empty);

            string storeLogoPath = GetCustomPortalDetails(PortalId)?.StoreLogo;
            rx1 = new Regex("#StoreLogo#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, storeLogoPath);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }
        private string GetServiceMail(int portalId)
        {
            IZnodeRepository<ZnodePortal> znodePortal = new ZnodeRepository<ZnodePortal>();
            IZnodeRepository<ZnodeDomain> znodePortalDomain = new ZnodeRepository<ZnodeDomain>();
            PortalModel model = new PortalModel();
            model = (from portal in znodePortal.Table
                     join domain in znodePortalDomain.Table.Where(x => x.PortalId == portalId && x.ApplicationType == ApplicationTypesEnum.WebStore.ToString() && x.IsActive && x.IsDefault) on portal.PortalId equals domain.PortalId into pf
                     from portalDomain in pf.DefaultIfEmpty()
                     where portal.PortalId == portalId
                     select new PortalModel
                     {

                         AdministratorEmail = portal.AdminEmail,
                         CustomerServiceEmail = portal.CustomerServiceEmail
                     }).FirstOrDefault();
            return model.CustomerServiceEmail;
        }
        /*Start Quote Lookup*/
        public virtual bool QuoteLookup(AWCTQuoteLookupModel model)
        {
            /*start mail to admin*/

            PortalModel Portalmodel = GetCustomPortalDetails(ZnodeConfigManager.SiteConfig.PortalId);
            //string senderEmail = Portalmodel?.AdministratorEmail;
            //string receiverEmail = Portalmodel?.AdministratorEmail;

            /*End send mail to admin*/
            string senderEmail = model.QuoteEmailAddress;
            string receiverEmail = model.QuoteEmailAddress;
            string apiUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string subject = "";
                //$"{senderEmail.Split('@').FirstOrDefault()}  wants you to see this item at {ZnodeConfigManager.SiteConfig.StoreName}";

            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("QuoteLookup", (PortalId > 0) ? PortalId : PortalId, 1);
            if (HelperUtility.IsNull(emailTemplateMapperModel))
            {
                throw (new Exception("Not Implemented"));
            }
            else
            {
                ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(emailTemplateMapperModel?.Descriptions);
                subject = emailTemplateMapperModel?.Subject;
                string messageText = GetEmailTemplate(model, receiptHelper);

                return SendMail(senderEmail, receiverEmail, subject, messageText, emailTemplateMapperModel.IsEnableBcc, model.QuoteEmailAddress, true);

            }

        }

        private string GetEmailTemplate(AWCTQuoteLookupModel model, ZnodeReceiptHelper receiptHelper)
        {
            string messageText = receiptHelper.Output;

            Regex rx1 = new Regex("#QuoteNumber#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.QuoteNumber);

            rx1 = new Regex("#QuoteEmailAddress#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(HelperUtility.IsNull(messageText) ? string.Empty : messageText, model.QuoteEmailAddress);

            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            return messageText;
        }
        /*End Quote Lookup*/
    }
}
