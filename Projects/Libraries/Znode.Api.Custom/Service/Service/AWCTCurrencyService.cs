﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTCurrencyService: CurrencyService
    {

        //Get currency details
        public virtual CurrencyModel GetCurrencyDetail(int portalId)
        {
            IZnodeRepository<ZnodeCurrency> znodeCurrency = new ZnodeRepository<ZnodeCurrency>();
            IZnodeRepository<ZnodePortalUnit> znodePortalUnit = new ZnodeRepository<ZnodePortalUnit>();

            CurrencyModel currencyModel = (from _znodePortalUnit in znodePortalUnit.Table
                                           join _znodeCurrency in znodeCurrency.Table on _znodePortalUnit.CurrencyId equals _znodeCurrency.CurrencyId
                                           where _znodePortalUnit.PortalId == portalId
                                           select new CurrencyModel
                                           {
                                               CurrencyCode = _znodeCurrency.CurrencyCode,
                                               CurrencyId = _znodeCurrency.CurrencyId
                                           })?.FirstOrDefault();

            return currencyModel;
        }
    }
}
