﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTPortalService : PortalService
    {
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;

        public AWCTPortalService()
        {
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
        }
        #region Generate Portal Images
        //Generate portal wise images.
        public override bool GenerateImages(int portalId)
        {

            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Portal.ToString(), TraceLevel.Info);
            try
            {
                //call the image utility scheduler 
                ZnodeLogging.LogMessage($"{Admin_Resources.InitiateImageUtility}", ZnodeLogging.Components.ImageScheduler.ToString(), TraceLevel.Info);

                ProcessStartInfo info = new ProcessStartInfo();
                string schedulerPath = GetSchedulerPath();
                info.FileName = !string.IsNullOrEmpty(schedulerPath) && File.Exists(schedulerPath) ? schedulerPath : System.Web.Hosting.HostingEnvironment.MapPath(ZnodeConstant.schedulerPath);

                FilterCollection filters = new FilterCollection();
                filters.Add(new FilterTuple(ZnodePortalCatalogEnum.PortalId.ToString(), ProcedureFilterOperators.Equals, Convert.ToString(portalId)));
                EntityWhereClauseModel whereClauseModel = DynamicClauseHelper.GenerateDynamicWhereClauseWithFilter(filters.ToFilterDataCollection());
                ZnodeLogging.LogMessage("WhereClause : ", ZnodeLogging.Components.Portal.ToString(), TraceLevel.Verbose, whereClauseModel);
                int chunkSize = 10000;//ZnodeConstant.ImageChunkSize;
                int localeId = 1;
                int catalogId = Convert.ToInt32(_portalCatalogRepository.GetEntity(whereClauseModel.WhereClause)?.PublishCatalogId);
                int loginUserId = GetLoginUserId();
                ZnodeLogging.LogMessage("catalogId and loginUserId", ZnodeLogging.Components.Warehouse.ToString(), TraceLevel.Verbose, new object[] { catalogId, loginUserId });
                string apiURL = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}";
                string tokenValue = string.IsNullOrEmpty(HttpContext.Current.Request.Headers["Token"]) ? "0" : HttpContext.Current.Request.Headers["Token"];
                info.Arguments = $"ImageHelper {portalId} {chunkSize} {loginUserId} {catalogId} {localeId} {apiURL} {0} {HttpContext.Current.Request.Headers["Authorization"]?.Replace("Basic ", "")} {tokenValue} {ZnodeApiSettings.RequestTimeout}";
                info.UseShellExecute = false;

                Process.Start(info);
                ZnodeLogging.LogMessage("Executed.", ZnodeLogging.Components.Portal.ToString(), TraceLevel.Info);
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage($"{Admin_Resources.ErrorProcessingImage}", ZnodeLogging.Components.ImageScheduler.ToString(), TraceLevel.Error, ex);

                return false;
            }
        }
        #endregion
    }
}
