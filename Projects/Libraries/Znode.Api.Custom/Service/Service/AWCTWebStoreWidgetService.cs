﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Helper;
using Znode.Api.Custom.IHelper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;


namespace Znode.Api.Custom.Service.Service
{
    public class AWCTWebStoreWidgetService : WebStoreWidgetService
    {
        #region Private Variables
        private readonly IMongoRepository<WidgetSliderBannerEntity> _sliderBannerMongoRepository;
        private readonly IMongoRepository<WidgetProductEntity> _widgetProductMongoRepository;
        private ProductMongoService _productMongoService;
        private readonly IMongoRepository<WidgetTitleEntity> _linkMongoRepository;
        private readonly IMongoRepository<WidgetCategoryEntity> _widgetCategoryMongoRepository;
        private CategoryMongoService _categoryMongoService;
        private IMongoRepository<ProductEntity> _productMongoRepository;
        private AWCTPublishProductService _publishProductService;
        private IMongoRepository<TextWidgetEntity> _widgetTextEntity;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private PublishProductHelper publishProductHelper = new PublishProductHelper();
        private IAWCTAttributeSwatchHelper _awctAttributeSwatchHelper;
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IZnodeRepository<ZnodePimLinkProductDetail> _linkDetailRepository;
        #endregion

        #region Constructor

        public AWCTWebStoreWidgetService() : base()
        {

            _widgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            _productMongoService = new ProductMongoService();
            //_linkMongoRepository = new MongoRepository<WidgetTitleEntity>();
            //_widgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            //_categoryMongoService = new CategoryMongoService();
            _productMongoRepository = new MongoRepository<ProductEntity>();
            _publishProductService = new AWCTPublishProductService();
            // _widgetTextEntity = new MongoRepository<TextWidgetEntity>();
            _awctAttributeSwatchHelper = new AWCTAttributeSwatchHelper();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>();
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            _linkDetailRepository = new ZnodeRepository<ZnodePimLinkProductDetail>();
        }

        #endregion

        #region PDP Frequently Bought Together Swatch Section
        public override WebStoreLinkProductListModel GetLinkProductList(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            try
            {

                WebStoreLinkProductListModel webStoreLinkProductListModel = new WebStoreLinkProductListModel();
                webStoreLinkProductListModel = base.GetLinkProductList(parameter, expands);
                webStoreLinkProductListModel = GetSwatchImagesForLinkProductsOnPDP(webStoreLinkProductListModel, parameter.PortalId);
                return webStoreLinkProductListModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("GetLinkProductList" + Convert.ToString(ex.StackTrace), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Error);
                return null;
            }
        }

        private WebStoreLinkProductListModel GetSwatchImagesForLinkProductsOnPDP(WebStoreLinkProductListModel webStoreLinkProductListModel, int portalId)
        {
            #region Commented code
            //try
            //{
            //    ImageHelper image = new ImageHelper(portalId);
            //    string ProductImageName = "";

            //    string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //    string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);
            //    string _imagePath = "";
            //    string _swatchImagePath = "";


            //    int index = 0;

            //    //ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //    index = ImageSmallPath.LastIndexOf('/');
            //    if (index != -1)
            //        _imagePath = ImageSmallPath.Substring(0, index) + "/";

            //    //OriginalImagepath = http://localhost:44762/Data/Media/
            //    index = OriginalImagepath.LastIndexOf('/');
            //    if (index != -1)
            //    {
            //        _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //        _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //    }


            //    int? _linkproductcount = webStoreLinkProductListModel.LinkProductList?.Count;
            //    for (int i = 0; i < _linkproductcount; i++)
            //    {
            //        List<ProductAlterNateImageModel> simage = GetSwatchImages(String.Join(",", webStoreLinkProductListModel.LinkProductList[i].PublishProduct.Select(x => x.PublishProductId).ToList()), _imagePath, _swatchImagePath, "Home");
            //        foreach (PublishProductModel prd1 in webStoreLinkProductListModel.LinkProductList[i].PublishProduct)
            //        {
            //            if (prd1.AlternateImages == null)
            //                prd1.AlternateImages = new List<ProductAlterNateImageModel>();
            //            prd1.AlternateImages = simage.Where(x => x.FileName == prd1.PublishProductId.ToString()).ToList<ProductAlterNateImageModel>();
            //        }
            //    }
            //}
            //catch(Exception ex)
            //{
            //    ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            //}
            #endregion
            AWCTAttributeSwatchHelper awctAttributeSwatchHelper = new Helper.AWCTAttributeSwatchHelper();
            int? _linkproductcount = webStoreLinkProductListModel.LinkProductList?.Count;
            for (int i = 0; i < _linkproductcount; i++)
            {
                awctAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "awctColorName");
            }
            return webStoreLinkProductListModel;

        }

        #endregion

        #region New Product widget
        public override WebStoreWidgetProductListModel GetProducts(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            List<WidgetProductEntity> productListWidgetEntity = _widgetProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(CreateFilterCollectionForProducts(parameter).ToFilterMongoCollection()));
            ZnodeLogging.LogMessage("productListWidgetEntity list count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, productListWidgetEntity?.Count());
            IMongoQuery query = Query.And(
            Query<CategoryEntity>.EQ(pr => pr.ZnodeCatalogId, parameter.PublishCatalogId),
            Query<CategoryEntity>.EQ(pr => pr.IsActive, true),
             Query<CategoryEntity>.EQ(pr => pr.LocaleId, parameter.LocaleId));
            ZnodeLogging.LogMessage("Mongo query to get categoryListWidgetEntity list: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, query);

            List<CategoryEntity> categoryListWidgetEntity = _categoryMongoRepository.GetEntityList(query).ToList();
            foreach (CategoryEntity item in categoryListWidgetEntity)
            {
                if ((item.ActivationDate == null || item.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (item.ExpirationDate == null || item.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                {
                    parameter.CategoryIds += item.ZnodeCategoryId.ToString() + ",";
                }
            }

            PublishProductListModel model = GetPublishProducts(expands, ProductListFilters(parameter, string.Join(",", productListWidgetEntity?.OrderBy(x => x.DisplayOrder)?.Select(x => x.ZnodeProductId))), null, null);
            if (model.PublishProducts != null && model.PublishProducts.Count > 0)
            {
                //Nivi code to assign swatch 
                model.PublishProducts = AssignSwatchImages(model?.PublishProducts, parameter.PortalId);
            }
            ZnodeLogging.LogMessage("Count of PublishProducts list in PublishProductListModel returned from method GetPublishProducts: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.PublishProducts?.Count);

            WebStoreWidgetProductListModel listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts, productListWidgetEntity);

            listModel.DisplayName = parameter.DisplayName;
            ZnodeLogging.LogMessage("DisplayName property of WebStoreWidgetProductListModel", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.DisplayName);
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return listModel;
        }
        private List<PublishProductModel> AssignSwatchImages(List<PublishProductModel> publishProducts, int portalId)
        {
            #region Commented Code
            //ImageHelper image = new ImageHelper(portalId);
            //string ProductImageName = "";

            //string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);
            //string _imagePath = "";
            //string _swatchImagePath = "";


            //int index = 0;

            ////ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //index = ImageSmallPath.LastIndexOf('/');
            //if (index != -1)
            //    _imagePath = ImageSmallPath.Substring(0, index) + "/";

            ////OriginalImagepath = http://localhost:44762/Data/Media/
            //index = OriginalImagepath.LastIndexOf('/');
            //if (index != -1)
            //{
            //    _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //    _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //}

            //List<ProductAlterNateImageModel> simage = GetSwatchImages(String.Join(",", publishProducts.Select(x => x.PublishProductId).ToList()), _imagePath, _swatchImagePath, "Home");
            //foreach (PublishProductModel prd in publishProducts)
            //{
            //    if (prd.AlternateImages == null)
            //        prd.AlternateImages = new List<ProductAlterNateImageModel>();
            //    prd.AlternateImages = simage.Where(x => x.FileName == prd.PublishProductId.ToString()).ToList<ProductAlterNateImageModel>();
            //}
            #endregion
            AWCTAttributeSwatchHelper awctAttributeSwatchHelper = new Helper.AWCTAttributeSwatchHelper();
            awctAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "awctColorName");
            return publishProducts;
        }
        private List<ProductAlterNateImageModel> GetSwatchImages(string PublishedProductId, string ImagePath, string SwatchPath, string PageName)
        {
            IZnodeViewRepository<ProductAlterNateImageModel> objStoredProc = new ZnodeViewRepository<ProductAlterNateImageModel>();

            objStoredProc.SetParameter("@PublishedProductList", PublishedProductId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@ImagePath", ImagePath, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SwatchPath", SwatchPath, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Case", PageName, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("AWCT_GetSwatchImages @PublishedProductList,@ImagePath,@SwatchPath,@Case").ToList();

        }
        private FilterCollection CreateFilterCollectionForProducts(WebStoreWidgetParameterModel parameter)
      => new FilterCollection() {
                new FilterTuple("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString()),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.WidgetsKey.ToString(), FilterOperators.Like, parameter.WidgetKey),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.TypeOFMapping.ToString(), FilterOperators.Like, parameter.TypeOfMapping),
        };
        //Generate filters to render product list.
        private FilterCollection ProductListFilters(WebStoreWidgetParameterModel parameter, string productIds)
        {
            FilterCollection filter = new FilterCollection();
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.In, productIds));
            filter.Add(new FilterTuple(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            filter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, parameter.PublishCatalogId.ToString()));
            if (!string.IsNullOrEmpty(parameter.CategoryIds))
            {
                filter.Add(new FilterTuple(FilterKeys.CategoryIds, FilterOperators.In, parameter.CategoryIds?.TrimEnd(',')));
            }

            if (parameter.ProductProfileId > 0)
            {
                filter.Add(new FilterTuple(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(parameter.ProductProfileId)));
            }

            return filter;
        }
        //Mapping data from List<PublishProductModel> model to widget product model.
        private WebStoreWidgetProductListModel ToWebstoreWidgetProductListModel(List<PublishProductModel> model, List<WidgetProductEntity> productListWidgetEntity)
        {
            WebStoreWidgetProductListModel webStoreWidgetProductListModel = new WebStoreWidgetProductListModel();
            webStoreWidgetProductListModel.Products = new List<WebStoreWidgetProductModel>();

            foreach (PublishProductModel data in model)
            {
                WebStoreWidgetProductModel webStoreWidgetProductModel = new WebStoreWidgetProductModel();
                webStoreWidgetProductModel.WebStoreProductModel = MapData(data, productListWidgetEntity);
                webStoreWidgetProductListModel.Products.Add(webStoreWidgetProductModel);

            }
            ZnodeLogging.LogMessage("webStoreWidgetProductListModel list count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, webStoreWidgetProductListModel?.Products?.Count);
            webStoreWidgetProductListModel.Products = webStoreWidgetProductListModel.Products?.OrderBy(x => x.WebStoreProductModel.DisplyOrder).ToList();
            return webStoreWidgetProductListModel;
        }
        //Map PublishProductModel to WebStoreProductModel.
        private WebStoreProductModel MapData(PublishProductModel model, List<WidgetProductEntity> productListWidgetEntity)
        {
            return new WebStoreProductModel
            {
                Attributes = model.Attributes,
                LocaleId = model.LocaleId,
                CatalogId = model.PublishedCatalogId,
                PublishProductId = model.PublishProductId,
                Name = model.Name,
                RetailPrice = model.RetailPrice,
                SalesPrice = model.SalesPrice,
                PromotionalPrice = model.PromotionalPrice,
                SKU = model.SKU,
                ProductReviews = model.ProductReviews,
                CurrencyCode = model.CurrencyCode,
                CultureCode = model.CultureCode,
                ImageMediumPath = model.ImageMediumPath,
                ImageSmallPath = model.ImageSmallPath,
                ImageSmallThumbnailPath = model.ImageSmallThumbnailPath,
                ImageThumbNailPath = model.ImageThumbNailPath,
                ImageLargePath = model.ImageLargePath,
                SEODescription = model.SEODescription,
                SEOTitle = model.SEOTitle,
                Rating = model.Rating,
                TotalReviews = model.TotalReviews,
                SEOUrl = model.SEOUrl,
                SEOKeywords = model.SEOKeywords,
                GroupProductPriceMessage = model.GroupProductPriceMessage,
                Promotions = model.Promotions,
                DisplyOrder = productListWidgetEntity?.Where(x => x.ZnodeProductId == model.PublishProductId)?.Select(x => x.DisplayOrder)?.FirstOrDefault(),
                AlternateImages = model.AlternateImages
            };
        }

        #endregion

        #region Frequently Brought Together Check 9.3.1

        ////Get list of link products.
        //public override WebStoreLinkProductListModel GetLinkProductList(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        //{
        //    ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
        //    ZnodeLogging.LogMessage("Input parameter WebStoreWidgetParameterModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, parameter);

        //    List<ProductEntity> model = _productMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(LinkProductListFilters(parameter).ToFilterMongoCollection()), true);
        //    IEnumerable<AttributeEntity> attributeLinkProduct = GetAttributeLinkedProductsList(model);

        //    WebStoreLinkProductListModel webStoreLinkProductListModel = new WebStoreLinkProductListModel();
        //    List<WebStoreLinkProductModel> linkProductList = new List<WebStoreLinkProductModel>();

        //    //Map link product data.
        //    MapLinkProductsnew(parameter, attributeLinkProduct, linkProductList, expands);
        //    webStoreLinkProductListModel.LinkProductList = linkProductList;
        //    ZnodeLogging.LogMessage("LinkProductList of WebStoreLinkProductListModel count: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, webStoreLinkProductListModel?.LinkProductList?.Count);
        //    ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
        //    return webStoreLinkProductListModel;
        //}
        //private FilterCollection LinkProductListFilters(WebStoreWidgetParameterModel parameter)
        //{
        //    FilterCollection filter = new FilterCollection();
        //    filter.Add(new FilterTuple(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, parameter.CMSMappingId.ToString()));
        //    filter.Add(new FilterTuple(ZnodeLocaleEnum.LocaleId.ToString(), FilterOperators.Equals, parameter.LocaleId.ToString()));
        //    int? versionIds = GetCatalogVersionId(Convert.ToInt32(parameter.PublishCatalogId), Convert.ToInt32(parameter.LocaleId));
        //    ZnodeLogging.LogMessage("Catalog versionIds returned from method GetCatalogVersionId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, versionIds);

        //    filter.Add(FilterKeys.VersionId, FilterOperators.Equals, versionIds.HasValue ? versionIds.Value.ToString() : "0");

        //    SetProductIndexFilter(filter);
        //    if (parameter.ProductProfileId > 0)
        //        filter.Add(new FilterTuple(WebStoreEnum.ProfileIds.ToString(), FilterOperators.In, Convert.ToString(parameter.ProductProfileId)));
        //    return filter;
        //}
        ////Get associated attribute linked products list for products 
        //private IEnumerable<AttributeEntity> GetAttributeLinkedProductsList(List<ProductEntity> model)
        //{
        //    IEnumerable<AttributeEntity> attributeLinkProduct = new List<AttributeEntity>();
        //    //If product is marked as obsolete, then do not pull results for any other link attributes other than Product Suggestions.
        //    attributeLinkProduct = !model.Any(x => Convert.ToBoolean(x.Attributes?.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.IsObsolete)?.AttributeValues))
        //        ? model.SelectMany(x => x.Attributes.Where(y => y.AttributeTypeName == ZnodeConstant.Link && y.AttributeCode != ZnodeConstant.ProductSuggestions))
        //        : model.SelectMany(x => x.Attributes.Where(y => y.AttributeTypeName == ZnodeConstant.Link && y.AttributeCode == ZnodeConstant.ProductSuggestions));
        //    return attributeLinkProduct;
        //}
        ////Filters for link products.
        //private FilterCollection LinkProductFilters(WebStoreWidgetParameterModel parameter, AttributeEntity item)
        //{
        //    FilterCollection filters = new FilterCollection();
        //    filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.In, item.AttributeValues);
        //    filters.Add(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString());
        //    filters.Add(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString());
        //    filters.Add(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue);
        //    filters.Add(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, Convert.ToString(parameter.PublishCatalogId));
        //    return filters;
        //}
        ////Get publish products.
        //public override PublishProductListModel GetPublishProducts(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        //    => _publishProductService.GetPublishProductList(expands, filters, sorts, page);
        ////Map link product data.
        //public void MapLinkProductsnew(WebStoreWidgetParameterModel parameter, IEnumerable<AttributeEntity> attributeLinkProduct, List<WebStoreLinkProductModel> linkProductList, NameValueCollection expands)
        //{
        //    try
        //    {
        //        foreach (AttributeEntity item in attributeLinkProduct)
        //        {
        //            if (!string.IsNullOrEmpty(item.AttributeValues))
        //            {
        //                WebStoreLinkProductModel data = new WebStoreLinkProductModel();

        //                PublishProductListModel publishProductListModel = GetPublishProducts(expands, LinkProductFilters(parameter, item), null, null);
        //                data.AttributeName = item.AttributeName;
        //                //Code  is done for showing display order
        //                int? pimProductId = _publishProductRepository.Table.FirstOrDefault(x => x.PublishProductId == parameter.CMSMappingId)?.PimProductId;

        //                List<ZnodePimLinkProductDetail> pimLinkProductDetail = _linkDetailRepository.Table.Where(x => x.PimParentProductId == pimProductId)?.ToList();

        //                IEnumerable<int?> producIds = pimLinkProductDetail.Select(x => x.PimProductId);

        //                List<ZnodePublishProduct> productlist = _publishProductRepository.Table.Where(x => producIds.Contains(x.PimProductId)).ToList();

        //                try
        //                {
        //                    publishProductListModel.PublishProducts.ForEach(product =>
        //                    {
        //                        ZnodePublishProduct publishProduct = productlist
        //                                    .FirstOrDefault(productdata => productdata.PublishProductId == product.PublishProductId);

        //                        ZnodePimLinkProductDetail linkDetails = pimLinkProductDetail.FirstOrDefault(x => x.PimProductId == publishProduct.PimProductId);

        //                        product.DisplayOrder = linkDetails.DisplayOrder;
        //                    });

        //                }
        //                catch (Exception ex)
        //                {
        //                    ZnodeLogging.LogMessage("MapLinkProduct INNER-" + ex.StackTrace, "Custom", TraceLevel.Error);
        //                }
        //                data.PublishProduct.AddRange(publishProductListModel.PublishProducts.OrderBy(x => x.DisplayOrder));
        //                linkProductList.Add(data);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("MapLinkProduct-" + ex.StackTrace, "Custom", TraceLevel.Error);
        //    }
        //}

        #endregion
    }
}
